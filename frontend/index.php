<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMS | Dashboard</title>
        
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="css/AdminLTE.min.css">
        <link rel="stylesheet" href="css/skin-black.min.css">
    </head>
    
	<body class="hold-transition skin-black sidebar-mini">
        <div class="wrapper">
		
			<!-- ****************************************START OF HEADER & SIDEBAR*********************************************-->
            <header class="main-header">
                <a href="index.php" class="logo">
                    <span class="logo-mini"><b>CMS</b></span>
                    <span class="logo-lg"><b>C</b>risis <b>M</b>gmt <b>S</b>ystem</span>
                </a>
                
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
                    </a>
                    
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-envelope-o"></i>
									<span class="label label-success">4</span>
                                </a>
                                
								<ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    
									<li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Test1
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>
                                                    <p>Message1</p>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Test2
                                                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                    </h4>
                                                    <p>Message2</p>
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Test3
                                                        <small><i class="fa fa-clock-o"></i> Today</small>
                                                    </h4>
                                                    <p>Message3</p>
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Test4
                                                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                    </h4>
                                                    <p>Message4</p>
                                                </a>
                                            </li>
                                           
											<li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Test5
                                                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                    </h4>
                                                    <p>Message5</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    
									<li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>
                            
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    
									<li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                <i class="fa fa-users text-aqua"></i> Notif1
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                <i class="fa fa-users text-red"></i> Long message won't work here
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                <i class="fa fa-shopping-cart text-green"></i> Notif2
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                <i class="fa fa-user text-red"></i> Password Changed
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-flag-o"></i>
									<span class="label label-danger">9</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Design some buttons
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Create a nice theme
                                                        <small class="pull-right">40%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Some task I need to do
                                                        <small class="pull-right">60%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">60% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Make beautiful transitions
                                                        <small class="pull-right">80%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">80% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="img/user1-128x128.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs">Daniel Wellington</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="img/user1-128x128.jpg" class="img-circle" alt="User Image">
                                        <p>Daniel Wellington</p>
                                    </li>
                                    
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="lockOut.php" class="btn btn-default btn-flat"><i class="fa fa-lock"></i> Lock</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            
			
			
			
			
			
			
			
			
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/user1-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        
						<div class="pull-left info">
                            <p>Daniel Wellington</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                            </span>
                        </div>
                    </form>
					
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
						
						<li>
							<a href="index.php">
								<i class="fa fa-dashboard"></i> 
								<span>Dashboard</span>
							</a>
						</li>
						
                        <li class="treeview">
                            <a href="#">
								<i class="fa fa-edit"></i> 
								<span>Incidents</span> 
								<i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu">
                                <li class="active">
									<a href="createIncident.php">
										<i class="fa fa-circle-o"></i>Create New Incident
									</a>
								</li>
                                <li class="active">
									<a href="incidentRecords.php">
										<i class="fa fa-circle-o"></i>Incident Records
									</a>
								</li>
                            </ul>
                        </li>
						
						<li class="treeview">
                            <a href="#">
								<i class="fa fa-briefcase"></i> <span>Agency</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu">
                                <li class="active">
									<a href="createAgency.php">
										<i class="fa fa-circle-o"></i>Create New Agency
									</a>
								</li>
                                <li class="active">
									<a href="listOfAgencies.php">
										<i class="fa fa-circle-o"></i>List of Agencies
									</a>
								</li>
                            </ul>
                        </li>
						
						<li class="treeview">
                            <a href="#">
								<i class="fa fa-heart"></i> <span>Assistance Types</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu">
                                <li class="active">
									<a href="createAssistance.php">
										<i class="fa fa-circle-o"></i>Create New Assistance Type
									</a>
								</li>
                                <li class="active">
									<a href="listOfAssistances.php">
										<i class="fa fa-circle-o"></i>List of Assistance Types
									</a>
								</li>
                            </ul>
                        </li>
						
						<li class="treeview">
                            <a href="#">
								<i class="fa fa-map-o text-white"></i> <span>Map</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu">
                                <li class="active">
									<a href="map.php">
										<i class="fa fa-fire-extinguisher text-red"></i>Fire
									</a>
								</li>
                                <li class="active">
									<a href="map.php">
										<i class="fa fa-fire text-orange"></i>Haze
									</a>
								</li>
								<li class="active">
									<a href="map.php">
										<i class="fa fa-cloud text-blue"></i>Weather
									</a>
								</li>
                            </ul>
                        </li>
						
                        <li class="header">LABELS</li>
                        <li><a href="#" ><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
						
                    </ul>
                </section>
            </aside>
            <!-- ****************************************END OF HEADER & SIDEBAR*********************************************-->
			
			
			
			
			
			
			
            <div class="content-wrapper">
			
				
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Version 1.0</small>
                    </h1>
                    
					<ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                
				
				
				
				
				
				
				<section class="content">
                    <div class="row">
                        <div class="col-md-7">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
								  <li class="active"><a href="#fire" data-toggle="tab">Fire</a></li>
								  <li><a href="#haze" data-toggle="tab">Haze</a></li>
								</ul>
								<div class="tab-content">
									
									<div class="active tab-pane" id="fire">
										<!-- Post -->
										<div class="post">
										  <div class="user-block">
											<img class="img-circle img-bordered-sm" src="img/user1-128x128.jpg" alt="user image">
												<span class="username">
												  <a href="#">Call Operator 1 (can put a random pic)</a>
												</span>
											<span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
										  </div>
										  <!-- /.user-block -->
										  <p>
											Lorem ipsum represents a long-held tradition for designers,
											typographers and the like. Some people hate it and argue for
											its demise, but others ignore the hate as they create awesome
											tools to help create filler text for everyone from bacon lovers
											to Charlie Sheen fans.
										  </p>
										  <img class="img-responsive"  src="img/fire1.jpg" alt="user image">

										  
										</div>
									<!-- /.post -->
									
									<!-- Post -->
									<div class="post">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm" src="img/user1-128x128.jpg" alt="user image">
											<span class="username">
											  <a href="#">Call Operator 2 (can put a random pic)</a>
											</span>
										<span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
									  </div>
									  <!-- /.user-block -->
									  <p>
										Lorem ipsum represents a long-held tradition for designers,
										typographers and the like. Some people hate it and argue for
										its demise, but others ignore the hate as they create awesome
										tools to help create filler text for everyone from bacon lovers
										to Charlie Sheen fans.
									  </p>
									  <img class="img-responsive"  src="img/fire2.jpg" alt="user image">

									</div>

									
								  </div>
								  <!-- /.tab-pane -->
								  
								  
								  <div class="tab-pane" id="haze">
									<!-- Post -->
									<div class="post">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm" src="img/user2-160x160.jpg" alt="user image">
											<span class="username">
											  <a href="#">Call Operator 1 (can put a random pic)</a>
											</span>
										<span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
									  </div>
									  <!-- /.user-block -->
									  <p>
										Lorem ipsum represents a long-held tradition for designers,
										typographers and the like. Some people hate it and argue for
										its demise, but others ignore the hate as they create awesome
										tools to help create filler text for everyone from bacon lovers
										to Charlie Sheen fans.
									  </p>
									  <img class="img-responsive"  src="img/haze1.jpg" alt="user image">

									  
									</div>
									<!-- /.post -->

									<!-- Post -->
									<div class="post">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm" src="img/user3-128x128.jpg" alt="user image">
											<span class="username">
											  <a href="#">Call Operator 2 (can put a random pic)</a>
											</span>
										<span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
									  </div>
									  <!-- /.user-block -->
									  <p>
										Lorem ipsum represents a long-held tradition for designers,
										typographers and the like. Some people hate it and argue for
										its demise, but others ignore the hate as they create awesome
										tools to help create filler text for everyone from bacon lovers
										to Charlie Sheen fans.
									  </p>
									  <img class="img-responsive"  src="img/haze2.jpg" alt="user image">

									</div>

									
								  </div>
								  <!-- /.tab-pane -->
								  
								  
								  
								</div>
								<!-- /.tab-content -->
							  </div>
							  <!-- /.nav-tabs-custom -->
						</div>
						<div class="col-md-5">
							<!-- AREA CHART -->
							<div class="box box-success">
								<div class="box-header with-border">
								<h3 class="box-title">24-Hour PSI (the chart is derived from JS)</h3>

								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
								</div>
								<div class="box-body">
								  <div class="chart">
									<canvas id="areaChart" style="height:250px"></canvas>
								  </div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
                    </div>
                </section>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
                <section class="content">
                    <div class="row">
                        <div class="clearfix visible-sm-block"></div>
                    </div>
                </section>
            </div>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
            
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; Perfect10</strong>
            </footer>
            
            <div class="control-sidebar-bg"></div>
        </div>
		
		
		
        <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="plugins/fastclick/fastclick.js"></script>
        <script src="js/app.min.js"></script>
        <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="plugins/chartjs/Chart.min.js"></script>
        <script src="js/pages/dashboard2.js"></script>
        <script src="js/demo.js"></script> <!-- CHANGE THIS -->
		<script>
		  $(function () {
			/* ChartJS
			 * -------
			 * Here we will create a few charts using ChartJS
			 */

			//--------------
			//- AREA CHART -
			//--------------

			// Get context with jQuery - using jQuery's .get() method.
			var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
			// This will get the first returned node in the jQuery collection.
			var areaChart = new Chart(areaChartCanvas);

			var areaChartData = {
			  labels: ["January", "February", "March", "April", "May", "June", "July"],
			  datasets: [
				{
				  label: "Electronics",
				  fillColor: "rgba(210, 214, 222, 1)",
				  strokeColor: "rgba(210, 214, 222, 1)",
				  pointColor: "rgba(210, 214, 222, 1)",
				  pointStrokeColor: "#c1c7d1",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "rgba(220,220,220,1)",
				  data: [65, 59, 80, 81, 56, 55, 40]
				},
				{
				  label: "Digital Goods",
				  fillColor: "rgba(60,141,188,0.9)",
				  strokeColor: "rgba(60,141,188,0.8)",
				  pointColor: "#3b8bba",
				  pointStrokeColor: "rgba(60,141,188,1)",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "rgba(60,141,188,1)",
				  data: [28, 48, 40, 19, 86, 27, 90]
				}
			  ]
			};

			var areaChartOptions = {
			  //Boolean - If we should show the scale at all
			  showScale: true,
			  //Boolean - Whether grid lines are shown across the chart
			  scaleShowGridLines: false,
			  //String - Colour of the grid lines
			  scaleGridLineColor: "rgba(0,0,0,.05)",
			  //Number - Width of the grid lines
			  scaleGridLineWidth: 1,
			  //Boolean - Whether to show horizontal lines (except X axis)
			  scaleShowHorizontalLines: true,
			  //Boolean - Whether to show vertical lines (except Y axis)
			  scaleShowVerticalLines: true,
			  //Boolean - Whether the line is curved between points
			  bezierCurve: true,
			  //Number - Tension of the bezier curve between points
			  bezierCurveTension: 0.3,
			  //Boolean - Whether to show a dot for each point
			  pointDot: false,
			  //Number - Radius of each point dot in pixels
			  pointDotRadius: 4,
			  //Number - Pixel width of point dot stroke
			  pointDotStrokeWidth: 1,
			  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			  pointHitDetectionRadius: 20,
			  //Boolean - Whether to show a stroke for datasets
			  datasetStroke: true,
			  //Number - Pixel width of dataset stroke
			  datasetStrokeWidth: 2,
			  //Boolean - Whether to fill the dataset with a color
			  datasetFill: true,
			  //String - A legend template
			  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
			  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			  maintainAspectRatio: true,
			  //Boolean - whether to make the chart responsive to window resizing
			  responsive: true
			};

			//Create the line chart
			areaChart.Line(areaChartData, areaChartOptions);
			});
		</script>
    </body>
</html>