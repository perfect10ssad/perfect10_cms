<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMS | Lockscreen</title>
        
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="css/AdminLTE.min.css">
    </head>
    <body class="hold-transition lockscreen">
        
        <div class="lockscreen-wrapper">
            <div class="lockscreen-logo">
                <b>C</b>risis <b>M</b>gmt <b>S</b>ystem
            </div>
            
			<div class="lockscreen-name">Daniel Wellington</div>
            
			<div class="lockscreen-item">
                <div class="lockscreen-image">
                    <img src="img/user1-128x128.jpg" alt="User Image">
                </div>
                <form class="lockscreen-credentials">
                    <div class="input-group">
                        <input type="password" class="form-control" placeholder="password">
                        <div class="input-group-btn">
                            <button type="button" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            
			<div class="help-block text-center">
                Enter your password to retrieve your session
            </div>
           
			<div class="text-center">
                <a href="login.php"><h4>Sign in as a different user</h4></a>
            </div>
            
			<div class="lockscreen-footer text-center">
                <strong>Copyright &copy; Perfect10</strong>
            </div>
        </div>
        
		
        <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>