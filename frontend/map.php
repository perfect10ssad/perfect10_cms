<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMS | Map</title>
        
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="css/AdminLTE.min.css">
        <link rel="stylesheet" href="css/skin-green-light.min.css">
    </head>
    
	<body class="hold-transition skin-green-light sidebar-mini">
        
		<div class="wrapper">
           <!-- ****************************************START OF HEADER & SIDEBAR*********************************************-->
		   <header class="main-header">
                <a href="index.php" class="logo">
                    <span class="logo-mini"><b>P10</b></span>
                    <span class="logo-lg"><b>P</b>erfect <b>10</b></span>
                </a>
				
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
                    </a>
                </nav>
            </header>
            
            <aside class="main-sidebar">
                <section class="sidebar">
                    
                    <ul class="sidebar-menu">
                        <li class="header">Map Categories</li>
                        <li>
							<a href="createAssistance.php">
								<i class="fa fa-fire-extinguisher text-red"></i><span>Fire</span>
							</a>
						</li>
                        <li>
							<a href="listOfAssistances.php">
								<i class="fa fa-fire text-orange"></i><span>Haze</span>
							</a>
						</li>
						<li>
							<a href="listOfAssistances.php">
								<i class="fa fa-cloud text-blue"></i><span>Weather</span>
							</a>
						</li>
                        
                        <li class="header">Stay updated</li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-forumbee text-red"></i> <span>Subscribe!</span></a></li>
                        
                    </ul>
                </section>
            </aside>
            
			<!-- ****************************************END OF HEADER & SIDEBAR*********************************************-->
			
            <div class="content-wrapper">
				<section>
				<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Subscribe to CMS</h4>
						  </div>
						  <div class="modal-body">
							Select your preference:
							
								<div class="box-body">
									<div class="form-group">
										<label for="name" class="col-sm-2 control-label"><input type="checkbox"> SMS</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="name" placeholder="Enter phone number">
										</div>
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="name" class="col-sm-2 control-label"><input type="checkbox"> Email</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="name" placeholder="Enter email address">
										</div>
									</div>
								</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-primary">Subscribe!</button>
						  </div>
						</div>
					  </div>
					</div>
				</section>
                <div id="googleMap"></div>
            </div>
            
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; Perfect10</strong>
            </footer>
            
            <div class="control-sidebar-bg"></div>
        </div>
        
		
        <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="plugins/fastclick/fastclick.js"></script>
        <script src="js/app.min.js"></script>
        <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="plugins/chartjs/Chart.min.js"></script>
        <script src="js/dashboard2.js"></script>
        <script src="js/demo.js"></script> <!-- CHANGE THIS -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbd_8690nKhYQcJv3wMzbZLiUXziJePkg&libraries=places"></script>
		<script>
			function initialize() {
			  var mapProp = {
				center:new google.maps.LatLng(1.30000,103.80000),
				zoom:11,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			  };
			  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
		<script>
			$('#myModal').on('shown.bs.modal', function () {
			  $('#myInput').focus()
			})
		</script>
    </body>
</html>