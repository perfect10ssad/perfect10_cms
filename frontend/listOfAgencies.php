<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMS | Agency</title>
        
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <style>
			.center-me {
				text-align:center;
			}
		</style>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="css/AdminLTE.min.css">
        <link rel="stylesheet" href="css/skin-black.min.css">
		<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    </head>
    
	<body class="hold-transition skin-black sidebar-mini">
        
		<div class="wrapper">
           <!-- ****************************************START OF HEADER & SIDEBAR*********************************************-->
		   <header class="main-header">
                <a href="index.php" class="logo">
                    <span class="logo-mini"><b>CMS</b></span>
                    <span class="logo-lg"><b>C</b>risis <b>M</b>gmt <b>S</b>ystem</span>
                </a>
				
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
                    </a>
					
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-envelope-o"></i>
									<span class="label label-success">4</span>
                                </a>
								
								<ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
									<li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                    </div>
													<h4>
                                                        Test1
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>
													<p>Message1</p>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
													<h4>
                                                        Test2
                                                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                    </h4>
													<p>Message2</p>
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                    </div>
													<h4>
                                                        Test3
                                                        <small><i class="fa fa-clock-o"></i> Today</small>
                                                    </h4>
													<p>Message3</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>
                            
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-bell-o"></i>
									<span class="label label-warning">10</span>
                                </a>
								
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                <i class="fa fa-users text-aqua"></i> Notif1
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                <i class="fa fa-users text-red"></i> Long message won't work here
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                <i class="fa fa-shopping-cart text-green"></i> Notif2
                                                </a>
                                            </li>
                                            
											<li>
                                                <a href="#">
                                                <i class="fa fa-user text-red"></i> Password Changed
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-flag-o"></i>
									<span class="label label-danger">9</span>
                                </a>
                                
								<ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        
                                        <ul class="menu">
                                            <li>
                                                
                                                <a href="#">
                                                    <h3>
                                                        Design some buttons
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                           
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Create a nice theme
                                                        <small class="pull-right">40%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Some task I need to do
                                                        <small class="pull-right">60%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">60% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="#">
                                                    <h3>
                                                        Make beautiful transitions
                                                        <small class="pull-right">80%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">80% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    
									<li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="img/user1-128x128.jpg" class="user-image" alt="User Image">
									<span class="hidden-xs">Daniel Wellington</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="img/user1-128x128.jpg" class="img-circle" alt="User Image">
                                        <p>Daniel Wellington</p>
                                    </li>
                                    
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="lockOut.php" class="btn btn-default btn-flat"><i class="fa fa-lock"></i> Lock</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/user1-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Daniel Wellington</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                            </span>
                        </div>
                    </form>
                    
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
						
						<li>
							<a href="index.php">
								<i class="fa fa-dashboard"></i> 
								<span>Dashboard</span>
							</a>
						</li>
						
                       <li class="treeview">
                            <a href="#">
								<i class="fa fa-edit"></i> <span>Incidents</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
							<ul class="treeview-menu menu-open">
                                <li class="active">
									<a href="createIncident.php">
										<i class="fa fa-circle-o"></i>Create New Incident
									</a>
								</li>
                                <li class="active">
									<a href="incidentRecords.php">
										<i class="fa fa-circle-o"></i>Incident Records
									</a>
								</li>
                            </ul>
                        </li>
						
						<li class="treeview active">
                            <a href="#">
								<i class="fa fa-briefcase"></i> <span>Agency</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu menu-open">
                                <li class="active">
									<a href="createAgency.php">
										<i class="fa fa-circle-o"></i>Create New Agency
									</a>
								</li>
                                <li class="active">
									<a href="listOfAgencies.php">
										<i class="fa fa-circle-o"></i>List of Agencies
									</a>
								</li>
                            </ul>
                        </li>
						
						<li class="treeview">
                            <a href="#">
								<i class="fa fa-heart-o"></i> <span>Assistance Types</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu menu-open">
                                <li class="active">
									<a href="createAssistance.php">
										<i class="fa fa-circle-o"></i>Create New Assistance Type
									</a>
								</li>
                                <li class="active">
									<a href="listOfAssistances.php">
										<i class="fa fa-circle-o"></i>List of Assistance Types
									</a>
								</li>
                            </ul>
                        </li>
						<li class="treeview">
                            <a href="#">
								<i class="fa fa-map-o text-white"></i> <span>Map</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
							<ul class="treeview-menu">
                                <li class="active">
									<a href="createAssistance.php">
										<i class="fa fa-fire-extinguisher text-red"></i>Fire
									</a>
								</li>
                                <li class="active">
									<a href="listOfAssistances.php">
										<i class="fa fa-fire text-orange"></i>Haze
									</a>
								</li>
								<li class="active">
									<a href="listOfAssistances.php">
										<i class="fa fa-cloud text-blue"></i>Weather
									</a>
								</li>
                            </ul>
                        </li>
                        
                        <li class="header">LABELS</li>
                        <li><a href="#" ><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
                    </ul>
                </section>
            </aside>
            <!-- ****************************************END OF HEADER & SIDEBAR*********************************************-->
            
			<div class="content-wrapper">
				
			
                <section class="content-header">
                    <h1>List of Agencies</h1>
                    <ol class="breadcrumb">
						<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><i class="fa fa-briefcase"></i> Agency</li>
						<li class="active"><a href="listOfAgencies.php">List Of Agencies</a></li>
                    </ol>
                </section>
                
				<section class="content">
                    <div class="row">
                        <div class="col-xs-12">
							<!-- /.box -->
                            <div class="box">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th>Name</th>
												<th>Contact</th>
												<th>Email</th>
												<th>Address</th>
												<th>AgencyTypes</th>
												<th> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>SCDF</td>
                                                <td>123</td>
                                                <td>scdf@hotmail.com</td>
                                                <td>Career Centre, HQ Singapore Civil Defence Force, 91 Ubi Avenue 4, Singapore 408827</td>
                                                <td>Emergency Ambulance, Rescue and Evacuation, Fire-Fighting</td>
												<td>
													<a href="updateAgency.php">
													<button type="submit" name="update" id="update" class="btn btn-danger" value="Update">
													Update
													</button>
													</a>
												</td>
                                            </tr>
                                            <tr>
                                                <td>Singapore Power</td>
                                                <td>456</td>
                                                <td>spservices@singaporepower.com.sg</td>
                                                <td>Tampines</td>
                                                <td>Gas Leak Control</td>
												<td>
													<a href="updateAgency.php">
														<button type="submit" name="update" id="update" class="btn btn-danger" value="Update">
														Update
														</button>
													</a>
												</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
												<th>Contact</th>
												<th>Address</th>
												<th>AgencyTypes</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
						</div>
					</div>
				</section>
			
			
			
			
			
			
			
				 
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			</div>
            
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; Perfect10</strong>
            </footer>
            
            <div class="control-sidebar-bg"></div>
        </div>
        
		
        <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="plugins/fastclick/fastclick.js"></script>
        <script src="js/app.min.js"></script>
        <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="plugins/chartjs/Chart.min.js"></script>
        <script src="js/pages/dashboard2.js"></script>
        <script src="js/demo.js"></script>
        <script src="plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="plugins/fastclick/fastclick.js"></script>
        <script src="js/app.min.js"></script>
        <script src="js/demo.js"></script>
        <script>
            $(function () {
              $("#example1").DataTable();
              $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
              });
            });
        </script>
		
    </body>
</html>