-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2016 at 02:42 AM
-- Server version: 5.6.25
-- PHP Version: 5.5.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perfect10`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE IF NOT EXISTS `agencies` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `number` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `number`) VALUES
(9, 'SCDF', ''),
(10, 'Singapore Power', ''),
(11, 'National Environmental Agency', ''),
(12, 'Housing Development Board', ''),
(13, 'Perfect10 - Test', '');

-- --------------------------------------------------------

--
-- Table structure for table `assistance_types`
--

CREATE TABLE IF NOT EXISTS `assistance_types` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `number` varchar(14) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assistance_types`
--

INSERT INTO `assistance_types` (`id`, `name`, `number`, `agency_id`) VALUES
(15, 'Emergency Ambulance', '995', 9),
(16, 'Evacuation and Medical Support', '91056750', 9),
(17, 'Fire Fighting', '91886596', 9),
(18, 'Non-Emergency Ambulance', '1777', 9),
(19, 'Police Emergency', '999', 9),
(20, 'Dengue Extermination', '18009336483', 11),
(21, 'Gas Leak Control', '18007521800', 10),
(23, 'Flood and Drain Obstructions', '18002846600', 12);

-- --------------------------------------------------------

--
-- Table structure for table `call_operators`
--

CREATE TABLE IF NOT EXISTS `call_operators` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `username` varchar(70) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL DEFAULT 'call_operator'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `call_operators`
--

INSERT INTO `call_operators` (`id`, `name`, `username`, `password`, `role`) VALUES
(1, 'Shou Xian', 'shouxian', '$2y$10$uMFnNv0PuLtledW7aB0Mk.8jnmwBc.td/19e0UOlyGf9CWulcckVa', 'call_operator'),
(2, 'Meha', 'meha', '$2y$10$NON4wePVQQgzE1qCpZ77qe4yopW1qSz6jkTQ0EkbYJkfGwmV9tYI.', 'call_operator'),
(3, 'Wei Jun', 'wjuno', '$2y$10$SoF58/xKMnhGtoziiLk.EeePfNflXurP6K4A.bF.XdhUayO9wahva', 'call_operator'),
(4, 'Admin1', 'admin1', '$2y$10$N/qAU0rX1aLU7xrylZPe5.waDbYogL1D9LmAVWwV73lWD4aEUOmvG', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `haze_psi_readings`
--

CREATE TABLE IF NOT EXISTS `haze_psi_readings` (
  `region` varchar(255) NOT NULL,
  `reading_datetime` datetime NOT NULL,
  `psi` int(11) NOT NULL COMMENT 'hourly 24 hour PSI reading',
  `pm25` int(11) NOT NULL COMMENT 'hourly 24 hour PM2.5 reading'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `haze_psi_readings`
--

INSERT INTO `haze_psi_readings` (`region`, `reading_datetime`, `psi`, `pm25`) VALUES
('NRS', '2016-03-30 21:00:00', 84, 87),
('NRS', '2016-03-30 22:00:00', 83, 78),
('NRS', '2016-03-30 23:00:00', 82, 72),
('NRS', '2016-03-31 08:00:00', 76, 62),
('NRS', '2016-03-31 09:00:00', 76, 34),
('NRS', '2016-03-31 18:00:00', 74, 32),
('NRS', '2016-03-31 19:00:00', 76, 34),
('NRS', '2016-03-31 20:00:00', 78, 36),
('NRS', '2016-03-31 21:00:00', 79, 37),
('NRS', '2016-03-31 22:00:00', 81, 38),
('NRS', '2016-03-31 23:00:00', 81, 39),
('NRS', '2016-04-01 10:00:00', 79, 37),
('NRS', '2016-04-01 11:00:00', 79, 37),
('NRS', '2016-04-02 09:00:00', 69, 28),
('NRS', '2016-04-02 13:00:00', 70, 28),
('NRS', '2016-04-02 14:00:00', 69, 28),
('NRS', '2016-04-02 15:00:00', 69, 28),
('NRS', '2016-04-02 16:00:00', 68, 27),
('NRS', '2016-04-02 17:00:00', 67, 26),
('NRS', '2016-04-02 18:00:00', 66, 26),
('NRS', '2016-04-02 20:00:00', 65, 24),
('NRS', '2016-04-02 21:00:00', 65, 24),
('NRS', '2016-04-06 14:00:00', 60, 20),
('NRS', '2016-04-12 14:00:00', 77, 35),
('NRS', '2016-04-12 15:00:00', 76, 35),
('NRS', '2016-04-12 16:00:00', 75, 34),
('NRS', '2016-04-12 17:00:00', 75, 33),
('NRS', '2016-04-12 18:00:00', 76, 34),
('NRS', '2016-04-12 19:00:00', 76, 34),
('NRS', '2016-04-12 20:00:00', 76, 34),
('NRS', '2016-04-12 21:00:00', 75, 34),
('NRS', '2016-04-12 22:00:00', 75, 33),
('NRS', '2016-04-12 23:00:00', 74, 33),
('NRS', '2016-04-13 00:00:00', 73, 32),
('NRS', '2016-04-13 01:00:00', 72, 31),
('NRS', '2016-04-13 02:00:00', 72, 30),
('NRS', '2016-04-13 03:00:00', 71, 30),
('NRS', '2016-04-13 04:00:00', 70, 29),
('NRS', '2016-04-13 05:00:00', 68, 27),
('NRS', '2016-04-13 06:00:00', 68, 27),
('NRS', '2016-04-13 09:00:00', 64, 24),
('rCE', '2016-03-30 21:00:00', 74, 73),
('rCE', '2016-03-30 22:00:00', 73, 72),
('rCE', '2016-03-30 23:00:00', 72, 71),
('rCE', '2016-03-31 08:00:00', 70, 66),
('rCE', '2016-03-31 09:00:00', 70, 29),
('rCE', '2016-03-31 18:00:00', 74, 32),
('rCE', '2016-03-31 19:00:00', 76, 34),
('rCE', '2016-03-31 20:00:00', 77, 35),
('rCE', '2016-03-31 21:00:00', 79, 36),
('rCE', '2016-03-31 22:00:00', 80, 37),
('rCE', '2016-03-31 23:00:00', 80, 38),
('rCE', '2016-04-01 10:00:00', 79, 37),
('rCE', '2016-04-01 11:00:00', 79, 37),
('rCE', '2016-04-02 09:00:00', 63, 23),
('rCE', '2016-04-02 13:00:00', 62, 22),
('rCE', '2016-04-02 14:00:00', 62, 22),
('rCE', '2016-04-02 15:00:00', 61, 21),
('rCE', '2016-04-02 16:00:00', 61, 21),
('rCE', '2016-04-02 17:00:00', 60, 20),
('rCE', '2016-04-02 18:00:00', 60, 20),
('rCE', '2016-04-02 20:00:00', 60, 20),
('rCE', '2016-04-02 21:00:00', 59, 19),
('rCE', '2016-04-06 14:00:00', 56, 16),
('rCE', '2016-04-12 14:00:00', 72, 31),
('rCE', '2016-04-12 15:00:00', 71, 30),
('rCE', '2016-04-12 16:00:00', 70, 29),
('rCE', '2016-04-12 17:00:00', 70, 29),
('rCE', '2016-04-12 18:00:00', 70, 28),
('rCE', '2016-04-12 19:00:00', 70, 29),
('rCE', '2016-04-12 20:00:00', 69, 28),
('rCE', '2016-04-12 21:00:00', 68, 27),
('rCE', '2016-04-12 22:00:00', 68, 27),
('rCE', '2016-04-12 23:00:00', 67, 26),
('rCE', '2016-04-13 00:00:00', 67, 26),
('rCE', '2016-04-13 01:00:00', 67, 26),
('rCE', '2016-04-13 02:00:00', 66, 25),
('rCE', '2016-04-13 03:00:00', 65, 25),
('rCE', '2016-04-13 04:00:00', 65, 24),
('rCE', '2016-04-13 05:00:00', 64, 23),
('rCE', '2016-04-13 06:00:00', 62, 22),
('rCE', '2016-04-13 09:00:00', 60, 20),
('rEA', '2016-03-30 21:00:00', 66, 67),
('rEA', '2016-03-30 22:00:00', 66, 60),
('rEA', '2016-03-30 23:00:00', 66, 60),
('rEA', '2016-03-31 08:00:00', 68, 68),
('rEA', '2016-03-31 09:00:00', 68, 27),
('rEA', '2016-03-31 18:00:00', 71, 30),
('rEA', '2016-03-31 19:00:00', 72, 30),
('rEA', '2016-03-31 20:00:00', 72, 31),
('rEA', '2016-03-31 21:00:00', 72, 31),
('rEA', '2016-03-31 22:00:00', 72, 31),
('rEA', '2016-03-31 23:00:00', 72, 31),
('rEA', '2016-04-01 10:00:00', 70, 29),
('rEA', '2016-04-01 11:00:00', 69, 28),
('rEA', '2016-04-02 09:00:00', 58, 19),
('rEA', '2016-04-02 13:00:00', 58, 18),
('rEA', '2016-04-02 14:00:00', 58, 18),
('rEA', '2016-04-02 15:00:00', 57, 18),
('rEA', '2016-04-02 16:00:00', 57, 17),
('rEA', '2016-04-02 17:00:00', 57, 17),
('rEA', '2016-04-02 18:00:00', 56, 17),
('rEA', '2016-04-02 20:00:00', 56, 16),
('rEA', '2016-04-02 21:00:00', 56, 17),
('rEA', '2016-04-06 14:00:00', 54, 15),
('rEA', '2016-04-12 14:00:00', 68, 27),
('rEA', '2016-04-12 15:00:00', 68, 27),
('rEA', '2016-04-12 16:00:00', 68, 27),
('rEA', '2016-04-12 17:00:00', 67, 27),
('rEA', '2016-04-12 18:00:00', 67, 26),
('rEA', '2016-04-12 19:00:00', 67, 26),
('rEA', '2016-04-12 20:00:00', 67, 26),
('rEA', '2016-04-12 21:00:00', 66, 25),
('rEA', '2016-04-12 22:00:00', 66, 26),
('rEA', '2016-04-12 23:00:00', 66, 25),
('rEA', '2016-04-13 00:00:00', 65, 25),
('rEA', '2016-04-13 01:00:00', 65, 24),
('rEA', '2016-04-13 02:00:00', 65, 24),
('rEA', '2016-04-13 03:00:00', 64, 24),
('rEA', '2016-04-13 04:00:00', 64, 24),
('rEA', '2016-04-13 05:00:00', 63, 23),
('rEA', '2016-04-13 06:00:00', 63, 23),
('rEA', '2016-04-13 09:00:00', 61, 21),
('rNO', '2016-03-30 21:00:00', 84, 114),
('rNO', '2016-03-30 22:00:00', 83, 86),
('rNO', '2016-03-30 23:00:00', 82, 75),
('rNO', '2016-03-31 08:00:00', 76, 59),
('rNO', '2016-03-31 09:00:00', 76, 34),
('rNO', '2016-03-31 18:00:00', 69, 28),
('rNO', '2016-03-31 19:00:00', 65, 24),
('rNO', '2016-03-31 20:00:00', 65, 24),
('rNO', '2016-03-31 21:00:00', 64, 23),
('rNO', '2016-03-31 22:00:00', 64, 23),
('rNO', '2016-03-31 23:00:00', 63, 23),
('rNO', '2016-04-01 10:00:00', 71, 29),
('rNO', '2016-04-01 11:00:00', 70, 29),
('rNO', '2016-04-02 09:00:00', 69, 28),
('rNO', '2016-04-02 13:00:00', 70, 28),
('rNO', '2016-04-02 14:00:00', 69, 28),
('rNO', '2016-04-02 15:00:00', 69, 28),
('rNO', '2016-04-02 16:00:00', 68, 27),
('rNO', '2016-04-02 17:00:00', 67, 26),
('rNO', '2016-04-02 18:00:00', 66, 26),
('rNO', '2016-04-02 20:00:00', 65, 24),
('rNO', '2016-04-02 21:00:00', 65, 24),
('rNO', '2016-04-06 14:00:00', 54, 15),
('rNO', '2016-04-12 14:00:00', 68, 27),
('rNO', '2016-04-12 15:00:00', 68, 27),
('rNO', '2016-04-12 16:00:00', 68, 27),
('rNO', '2016-04-12 17:00:00', 68, 27),
('rNO', '2016-04-12 18:00:00', 69, 28),
('rNO', '2016-04-12 19:00:00', 69, 28),
('rNO', '2016-04-12 20:00:00', 69, 28),
('rNO', '2016-04-12 21:00:00', 68, 28),
('rNO', '2016-04-12 22:00:00', 67, 27),
('rNO', '2016-04-12 23:00:00', 67, 26),
('rNO', '2016-04-13 00:00:00', 66, 26),
('rNO', '2016-04-13 01:00:00', 66, 25),
('rNO', '2016-04-13 02:00:00', 65, 25),
('rNO', '2016-04-13 03:00:00', 64, 24),
('rNO', '2016-04-13 04:00:00', 64, 24),
('rNO', '2016-04-13 05:00:00', 64, 24),
('rNO', '2016-04-13 06:00:00', 65, 24),
('rNO', '2016-04-13 09:00:00', 62, 22),
('rSO', '2016-03-30 21:00:00', 75, 76),
('rSO', '2016-03-30 22:00:00', 74, 75),
('rSO', '2016-03-30 23:00:00', 72, 74),
('rSO', '2016-03-31 08:00:00', 71, 62),
('rSO', '2016-03-31 09:00:00', 70, 29),
('rSO', '2016-03-31 18:00:00', 73, 32),
('rSO', '2016-03-31 19:00:00', 76, 34),
('rSO', '2016-03-31 20:00:00', 78, 36),
('rSO', '2016-03-31 21:00:00', 79, 37),
('rSO', '2016-03-31 22:00:00', 81, 38),
('rSO', '2016-03-31 23:00:00', 81, 39),
('rSO', '2016-04-01 10:00:00', 78, 36),
('rSO', '2016-04-01 11:00:00', 78, 36),
('rSO', '2016-04-02 09:00:00', 62, 22),
('rSO', '2016-04-02 13:00:00', 61, 21),
('rSO', '2016-04-02 14:00:00', 61, 21),
('rSO', '2016-04-02 15:00:00', 61, 21),
('rSO', '2016-04-02 16:00:00', 60, 20),
('rSO', '2016-04-02 17:00:00', 60, 20),
('rSO', '2016-04-02 18:00:00', 59, 19),
('rSO', '2016-04-02 20:00:00', 59, 19),
('rSO', '2016-04-02 21:00:00', 59, 19),
('rSO', '2016-04-06 14:00:00', 57, 17),
('rSO', '2016-04-12 14:00:00', 77, 35),
('rSO', '2016-04-12 15:00:00', 76, 35),
('rSO', '2016-04-12 16:00:00', 75, 34),
('rSO', '2016-04-12 17:00:00', 75, 33),
('rSO', '2016-04-12 18:00:00', 76, 34),
('rSO', '2016-04-12 19:00:00', 76, 34),
('rSO', '2016-04-12 20:00:00', 76, 34),
('rSO', '2016-04-12 21:00:00', 75, 34),
('rSO', '2016-04-12 22:00:00', 75, 33),
('rSO', '2016-04-12 23:00:00', 74, 33),
('rSO', '2016-04-13 00:00:00', 73, 32),
('rSO', '2016-04-13 01:00:00', 72, 31),
('rSO', '2016-04-13 02:00:00', 72, 30),
('rSO', '2016-04-13 03:00:00', 71, 30),
('rSO', '2016-04-13 04:00:00', 70, 29),
('rSO', '2016-04-13 05:00:00', 68, 27),
('rSO', '2016-04-13 06:00:00', 68, 27),
('rSO', '2016-04-13 09:00:00', 64, 24),
('rWE', '2016-03-30 21:00:00', 73, 103),
('rWE', '2016-03-30 22:00:00', 74, 99),
('rWE', '2016-03-30 23:00:00', 74, 81),
('rWE', '2016-03-31 08:00:00', 73, 58),
('rWE', '2016-03-31 09:00:00', 73, 32),
('rWE', '2016-03-31 18:00:00', 69, 28),
('rWE', '2016-03-31 19:00:00', 68, 27),
('rWE', '2016-03-31 20:00:00', 65, 25),
('rWE', '2016-03-31 21:00:00', 64, 24),
('rWE', '2016-03-31 22:00:00', 63, 23),
('rWE', '2016-03-31 23:00:00', 63, 23),
('rWE', '2016-04-01 10:00:00', 63, 23),
('rWE', '2016-04-01 11:00:00', 63, 23),
('rWE', '2016-04-02 09:00:00', 66, 26),
('rWE', '2016-04-02 13:00:00', 64, 24),
('rWE', '2016-04-02 14:00:00', 64, 24),
('rWE', '2016-04-02 15:00:00', 65, 24),
('rWE', '2016-04-02 16:00:00', 64, 23),
('rWE', '2016-04-02 17:00:00', 63, 22),
('rWE', '2016-04-02 18:00:00', 62, 22),
('rWE', '2016-04-02 20:00:00', 62, 22),
('rWE', '2016-04-02 21:00:00', 62, 22),
('rWE', '2016-04-06 14:00:00', 60, 20),
('rWE', '2016-04-12 14:00:00', 67, 26),
('rWE', '2016-04-12 15:00:00', 66, 26),
('rWE', '2016-04-12 16:00:00', 66, 26),
('rWE', '2016-04-12 17:00:00', 66, 25),
('rWE', '2016-04-12 18:00:00', 67, 26),
('rWE', '2016-04-12 19:00:00', 67, 26),
('rWE', '2016-04-12 20:00:00', 67, 26),
('rWE', '2016-04-12 21:00:00', 67, 26),
('rWE', '2016-04-12 22:00:00', 67, 26),
('rWE', '2016-04-12 23:00:00', 66, 26),
('rWE', '2016-04-13 00:00:00', 66, 25),
('rWE', '2016-04-13 01:00:00', 64, 24),
('rWE', '2016-04-13 02:00:00', 63, 23),
('rWE', '2016-04-13 03:00:00', 62, 22),
('rWE', '2016-04-13 04:00:00', 61, 21),
('rWE', '2016-04-13 05:00:00', 61, 20),
('rWE', '2016-04-13 06:00:00', 60, 20),
('rWE', '2016-04-13 09:00:00', 57, 17);

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE IF NOT EXISTS `incidents` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(255) NOT NULL,
  `location_lat` double DEFAULT NULL,
  `location_lng` double DEFAULT NULL,
  `description` text NOT NULL,
  `status` varchar(70) DEFAULT 'Ongoing',
  `reporter_name` varchar(70) NOT NULL,
  `reporter_mobile` varchar(20) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `incident_type_id` int(11) NOT NULL,
  `call_operator_id` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incidents`
--

INSERT INTO `incidents` (`id`, `name`, `location`, `location_lat`, `location_lng`, `description`, `status`, `reporter_name`, `reporter_mobile`, `datetime`, `incident_type_id`, `call_operator_id`, `level`) VALUES
(69, '', 'Lucky Plaza Orchard Road Singapore', 1.3045649, 103.8339296, 'Flooding since 4pm.\r\n\r\nDebris everywhere, trees falling. Terrible sight in orchard road.', 'Completed', 'Meha', '91234567', '2016-04-12 09:50:27', 8, 4, 1),
(70, '', '256 Yishun Street 51 Singapore', 1.416543, 103.8435413, 'Neighbour just had dengue fever and sent to the hospital', 'Completed', 'Shou Xian', '96677321', '2016-04-12 09:53:16', 10, 4, 1),
(71, '', 'Bugis Junction Singapore', 1.2996001, 103.8551282, 'Fire reported at bugis near the Gong Cha area.', 'Ongoing', 'Kok Keong', '91827382', '2016-04-12 09:56:48', 9, 4, 1),
(72, '', 'Eunos Crescent Singapore', 1.3221442, 103.9021157, 'Smell of gas is very strong near reported place. Please be advised', 'Ongoing', 'Wei Jun', '91873748', '2016-04-12 10:00:42', 11, 4, 1),
(73, '', '438 Choa Chu Kang Avenue 4 Singapore', 1.384161, 103.739702, 'There is a strong smell of gas leak in the reported area', 'Ongoing', 'Shou Xian', '91823773', '2016-04-12 10:10:37', 11, 4, 1),
(74, '', 'Singapore Flyer Singapore', 1.2895301, 103.8632483, 'Parts of the flyer is now submerged', 'Ongoing', 'Shou Xian', '98273748', '2016-04-12 10:15:35', 8, 4, 1),
(75, '', 'Singapore', 1.352083, 103.819836, 'Flood', 'Completed', 'Wei Jun', '91827374', '2016-04-12 10:20:36', 8, 4, 1),
(76, '', '159 Beach Road Singapore', 1.300123, 103.859646, 'Very strong gasoline smell', 'Completed', 'Shou Xian', '97847483', '2016-04-12 11:04:13', 11, 4, 1),
(77, '', 'Singapore', 1.352083, 103.819836, 'Reported home owner has been stung by a dengue mosquito', 'Completed', 'Stone Cold', '82736645', '2016-04-12 23:12:46', 10, 4, 1),
(78, '', '955 Hougang Avenue 9 Singapore', 1.376395, 103.878274, 'Dengue outbreak reported', 'Ongoing', 'Meha', '94853848', '2016-04-13 00:00:19', 10, 4, 1),
(79, '', 'Lot 1 Shoppers'' Mall Singapore', 1.3846308, 103.7450239, 'Flooding in basement', 'Ongoing', 'Maybelle', '92835758', '2016-04-13 00:01:12', 8, 4, 1),
(80, '', 'Katong Swim Complex Wilkinson Road Singapore', 1.3020515, 103.8861914, 'Dengue outbreak at pool area. Pool has been closed for investigation', 'Ongoing', 'Shawn', '92838293', '2016-04-13 00:03:01', 10, 4, 1),
(81, '', 'Lorong 24A Geylang Singapore', 1.3121446, 103.8840513, 'Strong indication of gas leakage in the area', 'Ongoing', 'Wei Jun', '90079403', '2016-04-13 00:04:38', 11, 4, 1),
(82, '', '42 Nanyang Avenue Singapore', 1.3453241, 103.6833211, 'Flooding at level B5 of block N2.2', 'Ongoing', 'Xuan Wei', '94728382', '2016-04-13 10:37:52', 8, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `incident_types`
--

CREATE TABLE IF NOT EXISTS `incident_types` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `icon` varchar(50) NOT NULL DEFAULT 'fa-fire-extinguisher',
  `icon_color` varchar(50) NOT NULL DEFAULT 'text-red'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incident_types`
--

INSERT INTO `incident_types` (`id`, `name`, `icon`, `icon_color`) VALUES
(8, 'Flood', 'fa-tint', 'text-blue'),
(9, 'Fire', 'fa-fire-extinguisher', 'text-maroon'),
(10, 'Dengue', 'fa-bug', 'text-green'),
(11, 'Gas Leak', 'fa-fire', 'text-olive');

-- --------------------------------------------------------

--
-- Table structure for table `incident_types_assistance_types`
--

CREATE TABLE IF NOT EXISTS `incident_types_assistance_types` (
  `id` int(11) NOT NULL,
  `incident_type_id` int(11) NOT NULL,
  `assistance_type_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incident_types_assistance_types`
--

INSERT INTO `incident_types_assistance_types` (`id`, `incident_type_id`, `assistance_type_id`) VALUES
(16, 8, 16),
(17, 8, 23),
(19, 9, 17),
(20, 10, 20),
(21, 11, 16),
(22, 11, 21);

-- --------------------------------------------------------

--
-- Table structure for table `message_templates`
--

CREATE TABLE IF NOT EXISTS `message_templates` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `text` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) NOT NULL,
  `mobile_number` varchar(8) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `mobile_number`, `email`) VALUES
(1, '97847496', NULL),
(20, NULL, 'renegod@hotmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agencies`
--
ALTER TABLE `agencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assistance_types`
--
ALTER TABLE `assistance_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `call_operators`
--
ALTER TABLE `call_operators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `haze_psi_readings`
--
ALTER TABLE `haze_psi_readings`
  ADD PRIMARY KEY (`region`,`reading_datetime`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_types`
--
ALTER TABLE `incident_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_types_assistance_types`
--
ALTER TABLE `incident_types_assistance_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_templates`
--
ALTER TABLE `message_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agencies`
--
ALTER TABLE `agencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `assistance_types`
--
ALTER TABLE `assistance_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `call_operators`
--
ALTER TABLE `call_operators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `incident_types`
--
ALTER TABLE `incident_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `incident_types_assistance_types`
--
ALTER TABLE `incident_types_assistance_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `message_templates`
--
ALTER TABLE `message_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
