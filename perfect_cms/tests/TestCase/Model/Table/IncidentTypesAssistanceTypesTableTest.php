<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncidentTypesAssistanceTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncidentTypesAssistanceTypesTable Test Case
 */
class IncidentTypesAssistanceTypesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.incident_types_assistance_types',
        'app.incident_types',
        'app.assistance_types',
        'app.agencies',
        'app.incidents',
        'app.call_operators'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IncidentTypesAssistanceTypes') ? [] : ['className' => 'App\Model\Table\IncidentTypesAssistanceTypesTable'];
        $this->IncidentTypesAssistanceTypes = TableRegistry::get('IncidentTypesAssistanceTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IncidentTypesAssistanceTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
