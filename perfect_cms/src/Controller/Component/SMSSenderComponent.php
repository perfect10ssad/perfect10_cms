<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class SMSSenderComponent extends Component
{

	public function sendToAgency($message, $incidentType){
		$this->IncidentTypesAssistanceTypes = TableRegistry::get('IncidentTypesAssistanceTypes');
		$this->AssistanceTypes = TableRegistry::get('AssistanceTypes');

		// first we extract the assistance type IDs from the many-to-many table
		$assistanceTypes = $this->IncidentTypesAssistanceTypes->find()->where(['incident_type_id'=>$incidentType]);

		$assistanceNumbers = array();

		// then we obtain the number for each particular assistance type
		foreach($assistanceTypes as $assistanceType) {
			$ATDetails = $this->AssistanceTypes->get($assistanceType->assistance_type_id);

			if(!is_null($ATDetails->number))
				array_push($assistanceNumbers, $ATDetails->number);
		}

		// this will send an sms to all agencies
		// send($message, $assistanceNumbers);
	}

	public function sendToSubscriber($message){
		// Retrieve numbers from subscribers array and send the message out to the users
		$this->Subscribers = TableRegistry::get('Subscribers');

        $subscriberMobile = $this->Subscribers->find()->where(function ($exp, $q) {
            return $exp->isNotNull('mobile_number');
        })->extract('mobile_number')->toArray();
        
        // append all number with singapore extension
        foreach ($subscriberMobile as &$m)
            $m = '65'.$m;

        if (sizeof($subscriberMobile) > 0) {
			$this->send($message, $subscriberMobile);
		}
	}

	// TODO: Add this
    public function send($message, $numbers)
    {
    	$debug = true;
		require_once('textmagic/TextMagicAPI.php');
		$api = new \TextMagicAPI(array(
		  "username" => "shouxianyong", 
		  "password" => "G9bp3xWdq1"
		));

		// Use this number for testing purposes. This is absolutely free.
		if($debug)
			$numbers = array(9991234567);

		$results = $api->send($message, $numbers, true);
    }
}