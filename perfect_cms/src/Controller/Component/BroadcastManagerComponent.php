<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class BroadcastManagerComponent extends Component
{
	// The other component your component uses
    public $components = ['TwitterSender', 'FacebookSender', 'SMSSender', 'EmailSender'];

    public function broadcastIncident($message, $incidentType)
    {
        $this->EmailSender->send('Incident Record', $message);

        $this->TwitterSender->tweet($message);
        $this->FacebookSender->post($message);
        
    	//$this->SMSSender->sendToAgency($message, $incidentType);
        $this->SMSSender->sendToSubscriber($message);
    }

    public function broadcast($title, $message)
    {
        $this->EmailSender->send($title, $message);
        //$this->SMSSender->sendToSubscriber($message);
        $this->TwitterSender->tweet($message);
        $this->FacebookSender->post($message);
    }
}