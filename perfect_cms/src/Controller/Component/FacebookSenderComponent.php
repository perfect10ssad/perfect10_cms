<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class FacebookSenderComponent extends Component
{
	// Facebook message component
    public function post($message)
    {
		//https://www.facebook.com/Perfect10-CMS-1122163414527337/
		  // init facebook object
		  $fb = new \Facebook\Facebook([
		    'app_id' => '1040392856033811',
		    'app_secret' => '49d26885a6ac4ec53be091f64d231bce',
		    'default_graph_version' => 'v2.5',
		    // APP access token
		    //'default_access_token' => '1040392856033811|jvKSqWXcPPMolRItmqAxSrE1KzQ', // optional
		  ]);

		  // Step 1: Authorize user
		  // Step 2: Get page access token
		  // Step 3: Get extended access token by exchanging page access token
		  // Step 4: Post with php sdk with extended token

		  // 30-day access token. should be enough for the demo
		  $page_access_token = 'CAAOyO1acWhMBAJ3toUZCemU1KyoS7FxmcJDbPjj7DFhOMXz17my4BtoZBJqyq2ZBCQXpyoYnMTRuJrZCeBIemdFfZA3BFp1Tp7hWHVhZAeZArtU8m9omfAudzVDTzOKUgoS2V7wxKH5V0U7ZBuv5nhFm3CxXBDuKqtZAvF4piPRnnnZBWZCaJpjFqioq3JwZATRjlzcZD';
		  $page_id = '1122163414527337';

		  // define your POST parameters (replace with your own values)
		  $params = array(
		    "access_token" => $page_access_token, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
		    "message" => $message
		  );
		   
		  // post to Facebook
		  // see: https://developers.facebook.com/docs/reference/php/facebook-api/
		  try {
		    $ret = $fb->post('/'.$page_id.'/feed', $params);
		  } catch(Exception $e) {
		    
		  }
    }
}