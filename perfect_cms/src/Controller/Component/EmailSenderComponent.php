<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class EmailSenderComponent extends Component
{
/*
    public function send($message)
    {
    	$this->Subscribers = TableRegistry::get('Subscribers');

        $subscribersEmail = $this->Subscribers->find()->where(function ($exp, $q) {
            return $exp->isNotNull('email');
        })->extract('email')->toArray();

        for($i = 0; $i < sizeof($subscribersEmail); $i ++) {
			$email = new Email('default');
			$email->from(['perfect10_cms@zoho.com' => 'Crisis Management System'])
			    ->to($subscribersEmail[$i])
			    ->subject('Incident Record')
			    ->emailFormat('html')
			    ->send('<p>Dear Subscriber, </p>'.$message.'</p> Regards,<br/>Perfect10 CMS Team');
		}
    }
*/
    
    public function send($subject, $message)
    {
        $this->Subscribers = TableRegistry::get('Subscribers');

        $subscribersEmail = $this->Subscribers->find()->where(function ($exp, $q) {
            return $exp->isNotNull('email');
        })->extract('email')->toArray();

        for($i = 0; $i < sizeof($subscribersEmail); $i ++) {
            $email = new Email('default');
            $email->from(['perfect10_cms@zoho.com' => 'Crisis Management System'])
                ->to($subscribersEmail[$i])
                ->subject($subject)
                ->emailFormat('html')
                ->send('<p>Dear Subscriber, </p>'.$message.'</p> Regards,<br/>Perfect10 CMS Team');
        }
    }
}