<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class ReportController extends AppController
{
    public $helpers = array('GoogleMap');   //Adding the helper
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->config('viewClassMap.pdf', 'CakePdf.Pdf');
    }

    public function beforeFilter(Event $event)
    {

    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {   
        $this->loadModel('Incidents');
        $this->loadModel('IncidentTypes');
        $this->loadModel('HazePsiReadings');

        $haze_readings = $this->HazePsiReadings->find()->where(['region' => 'NRS'])->order(['reading_datetime' => 'DESC']);
        $incidents = $this->Incidents->find()->contain(['IncidentTypes'])->where(['datetime >=' => new \DateTime('-30 minutes')]);

        $nrs_psi_readings = $nrs_pm25_readings = $nrs_timings = $ce_psi = $no_psi = $we_psi = $so_psi = $ea_psi = array();
        $ce_pm25 = $no_pm25 = $we_pm25 = $ea_pm25 = $so_pm25 = array();

        foreach($haze_readings as $index => $n) {
            array_push($nrs_psi_readings, $n->psi);
            array_push($nrs_pm25_readings, $n->pm25);
            array_push($nrs_timings, $n->reading_datetime->i18nFormat('d MMM HH:mm'));

            if($index >= 9) 
                break;
        }

        $chart_psi_readings = $this->HazePsiReadings->find()->order(['reading_datetime' => 'ASC']);

        foreach($chart_psi_readings as $index=> $n) {
            switch ($n->region) {
                case "rCE":
                    array_push($ce_psi, $n->psi);
                    array_push($ce_pm25, $n->pm25);
                break;
                case "rNO":
                    array_push($no_psi, $n->psi);
                    array_push($no_pm25, $n->pm25);
                break;
                case "rWE":
                    array_push($we_psi, $n->psi);
                    array_push($we_pm25, $n->pm25);
                break;
                case "rEA":
                    array_push($ea_psi, $n->psi);
                    array_push($ea_pm25, $n->pm25);
                break;
                case "rSO":
                    array_push($so_psi, $n->psi);
                    array_push($so_pm25, $n->pm25);
                break;
            }

            if($index >= 60) 
                break;
        }

        $chart_psi = array('ce_psi'=>$ce_psi, 'no_psi'=>$no_psi, 'we_psi'=>$we_psi, 'ea_psi'=>$ea_psi, 'so_psi'=>$so_psi);
        $chart_pm25 = array('ce_pm25'=>$ce_pm25, 'no_pm25'=>$no_pm25, 'we_pm25'=>$we_pm25, 'ea_pm25'=>$ea_pm25, 'so_pm25'=>$so_pm25);

        $incidentTypes = $this->IncidentTypes->find();

        $this->set(compact('haze_readings', 
                            'incidents', 
                            'nrs_psi_readings', 
                            'nrs_pm25_readings', 
                            'nrs_timings',
                            'chart_psi',
                            'chart_pm25',
                            'incidentTypes'));

        $this->viewBuilder()->layout('report');

        if(false) {
            $CakePdf = new \CakePdf\Pdf\CakePdf();
            $CakePdf->template('view', 'default');
            $CakePdf->viewVars(array('incidents'=> $incidents, 
                                     'haze_readings'=>$haze_readings, 
                                      'nrs_psi_readings'=>$nrs_psi_readings, 
                                      'nrs_pm25_readings'=>$nrs_pm25_readings, 
                                      'nrs_timings'=>$nrs_timings,
                                        'chart_psi'=>$chart_psi,
                                        'chart_pm25'=>$chart_pm25));
            // Or write it to file directly
            $pdf = $CakePdf->write(APP . 'Generated_Reports' . DS . 'incidents.pdf');
        }
    }

    public function view()
    {
        $this->loadModel('Incidents');
        $this->loadModel('IncidentTypes');
        $this->loadModel('HazePsiReadings');

        $haze_readings = $this->HazePsiReadings->find()->where(['region' => 'NRS'])->order(['reading_datetime' => 'DESC']);
        $incidents = $this->Incidents->find()->contain(['IncidentTypes'])->where(['datetime >=' => new \DateTime('-30 minutes')]);

        $nrs_psi_readings = $nrs_pm25_readings = $nrs_timings = $ce_psi = $no_psi = $we_psi = $so_psi = $ea_psi = array();
        $ce_pm25 = $no_pm25 = $we_pm25 = $ea_pm25 = $so_pm25 = array();

        foreach($haze_readings as $index => $n) {
            array_push($nrs_psi_readings, $n->psi);
            array_push($nrs_pm25_readings, $n->pm25);
            array_push($nrs_timings, $n->reading_datetime->i18nFormat('d MMM HH:mm'));

            if($index >= 9) 
                break;
        }

        $chart_psi_readings = $this->HazePsiReadings->find()->order(['reading_datetime' => 'ASC']);

        foreach($chart_psi_readings as $index=> $n) {
            switch ($n->region) {
                case "rCE":
                    array_push($ce_psi, $n->psi);
                    array_push($ce_pm25, $n->pm25);
                break;
                case "rNO":
                    array_push($no_psi, $n->psi);
                    array_push($no_pm25, $n->pm25);
                break;
                case "rWE":
                    array_push($we_psi, $n->psi);
                    array_push($we_pm25, $n->pm25);
                break;
                case "rEA":
                    array_push($ea_psi, $n->psi);
                    array_push($ea_pm25, $n->pm25);
                break;
                case "rSO":
                    array_push($so_psi, $n->psi);
                    array_push($so_pm25, $n->pm25);
                break;
            }

            if($index >= 60) 
                break;
        }

        $chart_psi = array('ce_psi'=>$ce_psi, 'no_psi'=>$no_psi, 'we_psi'=>$we_psi, 'ea_psi'=>$ea_psi, 'so_psi'=>$so_psi);
        $chart_pm25 = array('ce_pm25'=>$ce_pm25, 'no_pm25'=>$no_pm25, 'we_pm25'=>$we_pm25, 'ea_pm25'=>$ea_pm25, 'so_pm25'=>$so_pm25);

        $this->set(compact('haze_readings', 
                            'incidents', 
                            'nrs_psi_readings', 
                            'nrs_pm25_readings', 
                            'nrs_timings',
                            'chart_psi',
                            'chart_pm25'));
    }
}
