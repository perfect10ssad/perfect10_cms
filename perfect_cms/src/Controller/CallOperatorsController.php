<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CallOperators Controller
 *
 * @property \App\Model\Table\CallOperatorsTable $CallOperators
 */
class CallOperatorsController extends AppController
{

    public function login()
    {
        if (empty($this->request->session()->read('Auth.User'))) {
            // Set the layout.
            $this->viewBuilder()->layout('login');

            if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function lock()
    {
        $this->viewBuilder()->layout('lock');
        $this->set('lock', true);
        //return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('callOperators', $this->paginate($this->CallOperators));
        $this->set('_serialize', ['callOperators']);
    }

    /**
     * View method
     *
     * @param string|null $id Call Operator id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $callOperator = $this->CallOperators->get($id, [
            'contain' => ['Incidents']
        ]);
        $this->set('callOperator', $callOperator);
        $this->set('_serialize', ['callOperator']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $callOperator = $this->CallOperators->newEntity();
        if ($this->request->is('post')) {
            $callOperator = $this->CallOperators->patchEntity($callOperator, $this->request->data);
            if ($this->CallOperators->save($callOperator)) {
                $this->Flash->success(__('The call operator has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The call operator could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('callOperator'));
        $this->set('_serialize', ['callOperator']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Call Operator id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $callOperator = $this->CallOperators->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $callOperator = $this->CallOperators->patchEntity($callOperator, $this->request->data);
            if ($this->CallOperators->save($callOperator)) {
                $this->Flash->success(__('The call operator has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The call operator could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('callOperator'));
        $this->set('_serialize', ['callOperator']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Call Operator id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $callOperator = $this->CallOperators->get($id);
        if ($this->CallOperators->delete($callOperator)) {
            $this->Flash->success(__('The call operator has been deleted.'));
        } else {
            $this->Flash->error(__('The call operator could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
