<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class DashboardController extends AppController
{
    public function index()
    {
    	$this->loadModel('IncidentTypes');
    	$this->loadModel('HazePsiReadings');

    	$incidentTypes = $this->IncidentTypes->find()->contain(['Incidents' => ['CallOperators']]);

    	$this->set('incidentTypes', $incidentTypes);

    	$NRS = $this->HazePsiReadings->find()->where(['region' => 'NRS'])->order(['reading_datetime' => 'DESC']);

    	$nrs_psi_readings = array();
    	$nrs_pm25_readings = array();
    	$nrs_timings = array();


    	foreach($NRS as $index => $n) {
            // we prepend elements to the front of the array because of how
            // jscharts handle it's data in the array's order
    		array_unshift($nrs_psi_readings, $n->psi);
    		array_unshift($nrs_pm25_readings, $n->pm25);
    		array_unshift($nrs_timings, $n->reading_datetime->i18nFormat('d-M-Y HH:mm'));

    		if($index >= 9) 
    			break;
    	}

    	$this->set(compact('nrs_psi_readings', 'nrs_pm25_readings', 'nrs_timings'));
    }
}
