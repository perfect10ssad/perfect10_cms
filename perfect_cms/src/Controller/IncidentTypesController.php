<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IncidentTypes Controller
 *
 * @property \App\Model\Table\IncidentTypesTable $IncidentTypes
 */
class IncidentTypesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {

        $this->set('incidentTypes', $this->IncidentTypes->find()->contain(['AssistanceTypes']));
        $this->set('_serialize', ['incidentTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Incident Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incidentType = $this->IncidentTypes->get($id, [
            'contain' => ['AssistanceTypes', 'Incidents']
        ]);
        $this->set('incidentType', $incidentType);
        $this->set('_serialize', ['incidentType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incidentType = $this->IncidentTypes->newEntity();
        if ($this->request->is('post')) {
            $incidentType = $this->IncidentTypes->patchEntity($incidentType, $this->request->data);
            if ($this->IncidentTypes->save($incidentType)) {
                $this->Flash->success(__('The incident type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The incident type could not be saved. Please, try again.'));
            }
        }
        $assistanceTypes = $this->IncidentTypes->AssistanceTypes->find('list', ['limit' => 200]);
        $this->set(compact('incidentType', 'assistanceTypes'));
        $this->set('_serialize', ['incidentType']);

        $this->render('edit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Incident Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incidentType = $this->IncidentTypes->get($id, [
            'contain' => ['AssistanceTypes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incidentType = $this->IncidentTypes->patchEntity($incidentType, $this->request->data);
            if ($this->IncidentTypes->save($incidentType)) {
                $this->Flash->success(__('The incident type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The incident type could not be saved. Please, try again.'));
            }
        }
        $assistanceTypes = $this->IncidentTypes->AssistanceTypes->find('list', ['limit' => 200]);
        $this->set(compact('incidentType', 'assistanceTypes'));
        $this->set('_serialize', ['incidentType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Incident Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incidentType = $this->IncidentTypes->get($id);
        if ($this->IncidentTypes->delete($incidentType)) {
            $this->Flash->success(__('The incident type has been deleted.'));
        } else {
            $this->Flash->error(__('The incident type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
