<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Incidents Controller
 *
 * @property \App\Model\Table\IncidentsTable $Incidents
 */
class AssistanceTypesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $assistanceTypes = $this->AssistanceTypes->find('all')->contain(['Agencies']);
        $this->set('assistanceTypes', $assistanceTypes);
    }

    /**
     * View method
     *
     * @param string|null $id Incident id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $assistanceType = $this->AssistanceTypes->get($id, [
            'contain' => ['Agencies']
        ]);
        $this->set('assistanceType', $assistanceType);
        $this->set('_serialize', ['assistanceType']);
    }


    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $assistanceType = $this->AssistanceTypes->newEntity();

        if ($this->request->is('post')) {
            $assistanceType = $this->AssistanceTypes->patchEntity($assistanceType, $this->request->data);
            $this->log($assistanceType);
            
            
        
            if ($this->AssistanceTypes->save($assistanceType)) {
                $this->Flash->success(__('The Assistance Type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The Assistance Type could not be saved. Please, try again.'));
            }
        }

        $agencies = $this->AssistanceTypes->Agencies->find('list', ['limit' => 200]);
        $this->set(compact('assistanceType', 'agencies'));
        $this->set('_serialize', ['assistanceType']);


    }

    /**
     * Edit method
     *
     * @param string|null $id Incident id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assistanceType = $this->AssistanceTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assistanceType = $this->AssistanceTypes->patchEntity($assistanceType, $this->request->data);
            
            if ($this->AssistanceTypes->save($assistanceType)) {
                $this->Flash->success(__('The Assistance Type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The Assistance Type could not be saved. Please, try again.'));
            }
        }
 

        $agencies = $this->AssistanceTypes->Agencies->find('list', ['limit' => 200]);
        $this->set(compact('assistanceType', 'agencies'));
        $this->set('_serialize', ['assistanceType']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assistanceType = $this->AssistanceTypes->get($id);
        if ($this->AssistanceTypes->delete($assistanceType)) {
            $this->Flash->success(__('The assistance type has been deleted.'));
        } else {
            $this->Flash->error(__('The assistance type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
