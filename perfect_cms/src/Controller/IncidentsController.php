<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;

// includes the methods in broadcast_manager
/**
 * Incidents Controller
 *
 * @property \App\Model\Table\IncidentsTable $Incidents
 */
class IncidentsController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // this will allow public usage of the json web service
        if ($this->request->is('json')) {
            $this->Auth->allow(['index', 'rest']);
        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $incidents = $this->Incidents->find()->contain(['IncidentTypes'])->order(['datetime' => 'ASC']);

        $this->set('incidents', $incidents);
        $this->set('_serialize', ['incidents']);
    }

    public function rest(){
        $incidents = $this->Incidents->find()->contain(['IncidentTypes'])->order(['datetime' => 'ASC'])->where(['status' => 'Ongoing']);
        $this->set('incidents', $incidents);
        $this->set('_serialize', ['incidents']);
    }

    /**
     * View method
     *
     * @param string|null $id Incident id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => ['IncidentTypes', 'CallOperators']
        ]);
        $this->set('incident', $incident);
        $this->set('_serialize', ['incident']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incident = $this->Incidents->newEntity();

                    // check if there is a duplicate entity
            $duplicateEntity = $this->Incidents->find()->where(['location' => 'Singapore', 'incident_type_id' => 1]);
        if ($this->request->is('post')) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);
            $incident->status = "Ongoing";
            $current_time = new \DateTime("now");
            $incident->datetime = $current_time->format("Y/m/d H:i:s");
            $incident->call_operator_id = $this->request->session()->read('Auth.User.id');

            // check if there is a duplicate entity
            $duplicateEntity = $this->Incidents->find()->where(['location' => $incident->location, 'incident_type_id' => $incident->incident_type_id]);

            $saveIncident = true;

            // if there is such an entity(or entities), then we check if the time is 30 minutes ago or not
            if (!empty($duplicateEntity)) {
                foreach($duplicateEntity as $dupe){
                    // if reported time was within last 30 minutes Or previous status is ongoing
                    if($dupe->datetime->wasWithinLast('30 minutes') || $dupe->status == "Ongoing") {
                        // don't save the incident
                        $saveIncident = false;
                        $this->Flash->warning(__('An incident with this location and incident type has been reported recently. To prevent duplicates, this record will not be saved'));
                        break;
                    }
                }
            }

            if($saveIncident) {
                if ($this->Incidents->save($incident)) {
                    // broadcasting functions
                    $this->loadModel('IncidentTypes');
                    $incidentType = $this->IncidentTypes->get($incident->incident_type_id);

                    $message = "A new incident has been reported. Category: " .$incidentType->name. " , Location: ". $incident->location;

                    // loads the broadcastmanager and broadcasts the message
                    $this->loadComponent('BroadcastManager');

                    $this->BroadcastManager->broadcastIncident($message, $incidentType->id);

                    $this->Flash->success(__('The incident has been created.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The incident could not be created. Please, try again.'));
                }
            }
        }
        $incidentTypes = $this->Incidents->IncidentTypes->find('list', ['limit' => 200]);
        $callOperators = $this->Incidents->CallOperators->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'incidentTypes', 'callOperators'));
        $this->set('_serialize', ['incident']);

        $this->set('title', 'Create Incident');
    }

    /**
     * Edit method
     *
     * @param string|null $id Incident id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);
            $incident->call_operator_id = $this->request->session()->read('Auth.User.id');
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been updated successfully.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The incident could not be updated. Please, try again.'));
            }
        }
        $incidentTypes = $this->Incidents->IncidentTypes->find('list', ['limit' => 200]);
        $callOperators = $this->Incidents->CallOperators->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'incidentTypes', 'callOperators'));
        $this->set('_serialize', ['incident']);

        $this->set('title', 'Update Incident');
        $this->render('add');
    }

    public function resolve($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => ['IncidentTypes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);

            // if the current status is equals to ongoing
            if( strcmp($incident->status, 'Ongoing') == 0 ) {
                $incident->status = 'Completed';
                if ($this->Incidents->save($incident)) {
                    $this->loadComponent('BroadcastManager');
                    
                    $message = "A crisis has been resolved. Location: ".$incident->location.". Type: ".$incident->incident_type->name;

                    $this->BroadcastManager->broadcastIncident($message, $incident->incident_type_id);
                    $this->Flash->success(__('The incident has been marked as completed.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The incident could not be updated. Please, try again.'));
                }
            } else {
                $this->Flash->warning(__('The incident has already been marked as completed.'));
            }
        }
        $incidentTypes = $this->Incidents->IncidentTypes->find('list', ['limit' => 200]);
        $callOperators = $this->Incidents->CallOperators->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'incidentTypes', 'callOperators'));
        $this->set('_serialize', ['incident']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incident = $this->Incidents->get($id);
        if ($this->Incidents->delete($incident)) {
            $this->Flash->success(__('The incident has been deleted.'));
        } else {
            $this->Flash->error(__('The incident could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
