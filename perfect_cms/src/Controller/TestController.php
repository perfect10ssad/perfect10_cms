<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Http\Client;
use Cake\Utility\Xml;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * Incidents Controller
 *
 * @property \App\Model\Table\IncidentsTable $Incidents
 */
class TestController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Incidents');
        $incidents = $this->Incidents->find()->where(['datetime >=' => date('Y-m-d H:i:s', strtotime('-30 minute'))]);

        $this->set(compact('incidents'));
    }

    /**
     * View method
     *
     * @param string|null $id Incident id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incident = $this->Incidents->newEntity();

        if ($this->request->is('post')) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);
            $incident->status = "Test";
            $incident->datetime = date("Y/m/d h:i:s");
            $incident->call_operator_id = $this->request->session()->read('Auth.User.id');
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The incident could not be saved. Please, try again.'));
            }
        }
        $incidentTypes = $this->Incidents->IncidentTypes->find('list', ['limit' => 200]);
        $callOperators = $this->Incidents->CallOperators->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'incidentTypes', 'callOperators'));
        $this->set('_serialize', ['incident']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Incident id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);
            $incident->call_operator_id = $this->request->session()->read('Auth.User.id');
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The incident could not be saved. Please, try again.'));
            }
        }
        $incidentTypes = $this->Incidents->IncidentTypes->find('list', ['limit' => 200]);
        $callOperators = $this->Incidents->CallOperators->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'incidentTypes', 'callOperators'));
        $this->set('_serialize', ['incident']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incident = $this->Incidents->get($id);
        if ($this->Incidents->delete($incident)) {
            $this->Flash->success(__('The incident has been deleted.'));
        } else {
            $this->Flash->error(__('The incident could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
