<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        // Redirects the user to Incidents index after logging in
        // and to the login page after logging out
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Incidents',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'CallOperators',
                'action' => 'login'
            ]
        ]);
    }

    public function beforeFilter(Event $event)
    {
        // Changing the controller for authentication to CallOperators instead of CallOperators
        $this->Auth->config('loginAction', ['controller' => 'CallOperators', 'action' => 'login']);
        // Changing the model for authentication to CallOperators instead of CallOperators
        $this->Auth->config('authenticate', [
            'Basic' => ['userModel' => 'CallOperators'],
            'Form' => ['userModel' => 'CallOperators']
        ]);

        // if the $lock variable is set
        if ( isset($lock) ) {
            // direct to the lock page
            return $this->redirect(
                ['controller' => 'CallOperators', 'action' => 'lock']
            );
        }

        if (!empty($this->request->session()->read('Auth.User'))){
            $this->set('role', $this->request->session()->read('Auth.User.role'));
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
