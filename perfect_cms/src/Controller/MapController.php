<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class MapController extends AppController
{
    public $helpers = array('GoogleMap');   //Adding the helper

    public function beforeFilter(Event $event)
    {
        // this will allow the map to be viewed without logging in
        $this->Auth->allow();
    }

    public function index()
    {
        $this->viewBuilder()->layout('map');

        $this->loadModel('Incidents');
        $this->loadModel('IncidentTypes');
        $this->loadModel('Subscribers');


        $incidents = $this->Incidents->find()->where(['status' => 'Ongoing'])->contain(['IncidentTypes']);
        $incidentTypes = $this->IncidentTypes->find();

        $subscriber = $this->Subscribers->newEntity();
        if ($this->request->is('post')) {
            $subscriber = $this->Subscribers->patchEntity($subscriber, $this->request->data);
            if ($this->Subscribers->save($subscriber)) {
                return $this->redirect(['action' => 'index']);
            } else {
            }
        }

        $this->set(compact('incidents', 'incidentTypes', 'subscriber'));
    }

    public function category($id = null)
    {
        $this->viewBuilder()->layout('map');
        if (!empty($this->request->params['pass'])) {
            $this->loadModel('Incidents');
            $this->loadModel('IncidentTypes');
            $this->loadModel('Subscribers');

            $key = $this->request->params['pass'][0];

            $result = $this->Incidents->find('all')->where(['incident_type_id' => $key, 'status'=>'Ongoing'])->contain(['IncidentTypes']);
            $incidentTypes = $this->IncidentTypes->find();

            $this->set('incidents', $result);
            $this->set(compact(['incidents', 'incidentTypes', 'subscriber']));

            $this -> render('index');
        } else {
            return $this->redirect('/map');
        }
    }
}