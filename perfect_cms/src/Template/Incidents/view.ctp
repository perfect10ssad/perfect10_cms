<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Incident'), ['action' => 'edit', $incident->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Incident'), ['action' => 'delete', $incident->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incident Types'), ['controller' => 'IncidentTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident Type'), ['controller' => 'IncidentTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Call Operators'), ['controller' => 'CallOperators', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Call Operator'), ['controller' => 'CallOperators', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="incidents view large-9 medium-8 columns content">
    <h3><?= h($incident->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Location') ?></th>
            <td><?= h($incident->location) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($incident->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Reporter Name') ?></th>
            <td><?= h($incident->reporter_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Incident Type') ?></th>
            <td><?= $incident->has('incident_type') ? $this->Html->link($incident->incident_type->name, ['controller' => 'IncidentTypes', 'action' => 'view', $incident->incident_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Call Operator') ?></th>
            <td><?= $incident->has('call_operator') ? $this->Html->link($incident->call_operator->name, ['controller' => 'CallOperators', 'action' => 'view', $incident->call_operator->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($incident->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Reporter Mobile') ?></th>
            <td><?= $this->Number->format($incident->reporter_mobile) ?></td>
        </tr>
        <tr>
            <th><?= __('Datetime') ?></th>
            <td><?= h($incident->datetime) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($incident->description)); ?>
    </div>
</div>
