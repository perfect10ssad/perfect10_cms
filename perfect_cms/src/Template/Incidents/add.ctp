<?= $this->element('content-header', ['title' => $title]);
    $this->fetch('content-header');
    ?>
<?= $this->Html->script('waitingfor'); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">New Incident</h3>
                </div>
                
                <?= $this->Form->create($incident, ['class' => 'form-horizontal', 'id'=>'createForm']) ?>
                <!-- <form class="form-horizontal">-->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="reporter_name" class="col-sm-2 control-label">Reporter's Name</label>
                            <div class="col-sm-8">
                                <?= $this->Form->text('reporter_name', ['class' => 'form-control', 'placeholder' => 'Enter the name of the reporter']) ?>
                                <!-- input type="email" class="form-control" id="name" placeholder="e.g. Sally" -->
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="reporter_mobile" class="col-sm-2 control-label">Reporter's Contact</label>
                            <div class="col-sm-8">
                                <?= $this->Form->text('reporter_mobile', ['class' => 'form-control', 'title'=>'Requires 8 characters and all digits', 'placeholder' => 'Enter the reporter\'s number']) ?>
                                <!-- input type="text" class="form-control" id="contact" placeholder="e.g. 91112222" -->
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="incidentType" class="col-sm-2 control-label">Incident Type</label>
                            <div class="col-sm-8">
                                <?= $this->Form->input('incident_type_id', [ 'label' => false, 'class' => 'form-control', 'options' => $incidentTypes]); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="location" class="col-sm-2 control-label">Location</label>
                            <div class="col-sm-8">
                                <?= $this->Form->input('location', 
                                                      ['id'=>'geocomplete', 
                                                        'label'=>false, 
                                                        'class' => 'form-control', 
                                                        'placeholder' => 'Type in an address']); ?>
                            </div>
                        </div>

                        <div class="form-group incident-map">
                          <div class="map-canvas col-sm-12" style="height:300px;"></div>
                          <?= $this->Form->input('location_lat', 
                                                      ['class' => 'form-control', 
                                                        'type' => 'hidden',
                                                        'data-geo' => 'lat', // this is necessary to populate details from autocomplete
                                                        'placeholder' => 'Hidden lat input']); ?>
                          <?= $this->Form->input('location_lng', 
                                                      [
                                                        'class' => 'form-control', 
                                                        'type' => 'hidden',
                                                        'data-geo' => 'lng', // same as above
                                                        'placeholder' => 'Hidden lng input']); ?>                          
                        </div>
                        
                        <div class="form-group">
                            <label for="incidentDesc" class="col-sm-2 control-label">Incident Description</label>
                            <div class="col-sm-8">
                                <?= $this->Form->textarea('description', ['lines' => 5, 'class' => 'form-control', 'placeholder' => 'Enter a short description of the problem/crisis encountered']) ?>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="box-footer">
                        <?= $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-default']) ?>
                        <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success pull-right', 'onclick'=>'waitingDialog.show();']) ?>
                    </div>
                <?= $this->Form->end() ?>
                <!-- /form -->
            </div>
        </div>
            
        <div class="clearfix visible-sm-block"></div>
    </div>

    <div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-header">
            <h1>Processing...</h1>
        </div>
        <div class="modal-body">
            <div class="progress progress-striped active">
                <div class="bar" style="width: 100%;"></div>
            </div>
        </div>
    </div>

</section>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbd_8690nKhYQcJv3wMzbZLiUXziJePkg&libraries=places"></script>
    <?= $this->Html->script('jquery.geocomplete') ?>
      <script>
      $(function(){
        // Sample style taken from https://snazzymaps.com/style/70/unsaturated-browns
        var mapStyles = [{"elementType":"geometry","stylers":[{"hue":"#ff4400"},{"saturation":-68},{"lightness":-4},{"gamma":0.72}]},{"featureType":"road","elementType":"labels.icon"},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"hue":"#0077ff"},{"gamma":3.1}]},{"featureType":"water","stylers":[{"hue":"#00ccff"},{"gamma":0.44},{"saturation":-33}]},{"featureType":"poi.park","stylers":[{"hue":"#44ff00"},{"saturation":-23}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"hue":"#007fff"},{"gamma":0.77},{"saturation":65},{"lightness":99}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"gamma":0.11},{"weight":5.6},{"saturation":99},{"hue":"#0091ff"},{"lightness":-86}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"lightness":-48},{"hue":"#ff5e00"},{"gamma":1.2},{"saturation":-23}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"saturation":-64},{"hue":"#ff9100"},{"lightness":16},{"gamma":0.47},{"weight":2.7}]}];

        // Options for the map
        var options = {
          map: ".map-canvas", // this will link the div with this class name to the map
          details: ".incident-map",
          detailsAttribute: "data-geo",
          componentRestrictions: {country: "SG"},
          mapOptions: {
            styles: mapStyles
          },
          <?php if($incident->isNew()) { ?>
            location: "Singapore" // Starting location of the map
          <?php } else { ?>
            center: {lat: <?= $incident->location_lat ?>,lng: <?= $incident->location_lng ?>},
          <?php } ?>
        };


        $("#geocomplete").geocomplete(options);

		
		$.validator.addMethod(
			"regex",
			function(value, element, regexp) {
				var check = false;
				return this.optional(element) || regexp.test(value);
			},
			"This field consists of invalid characters."
		);

		
        $( "#createForm" ).validate({
          rules: {
			reporter_name:{
				required: true,
				regex: /^[a-zA-Z][a-zA-Z_\s]+$/
			},
            reporter_mobile: {
              required: true,
			  digits: true,
              minlength: 8,
              maxlength: 8
            },
          }, 
          // hide modal popup if the valiation fails
          invalidHandler: function(event, validator) {
                waitingDialog.hide();
          }
        });

      });

    </script>
