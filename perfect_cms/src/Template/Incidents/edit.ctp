<?= $this->element('content-header', ['title' => 'Update Incident']);
    $this->fetch('content-header'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Incident Information</h3>
                </div>
                <?= $this->Form->create($incident, ['class' => 'form-horizontal', 'id'=>'updateForm']) ?>
                <!-- form class="form-horizontal" -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="status" class="col-sm-4 control-label">Status</label>
                            <div class="col-sm-4">
                                <?= $this->Form->select(
                                    'status',
                                    // Value => Display value
                                    ['Ongoing' => 'Ongoing', 'Completed' => 'Completed'],
                                    ['class' => 'form-control']
                                ); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Reporter Name</label>
                            <div class="col-sm-4">
                                <?= $this->Form->input('reporter_name', [ 'label' => false, 
                                                                'class' => 'form-control', 
                                                                'placeholder' => 'e.g. Sally' ]); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="contact" class="col-sm-4 control-label">Contact</label>
                            <div class="col-sm-4">
                                <?= $this->Form->input('reporter_mobile', [ 'label' => false, 
                                                                'class' => 'form-control', 'type' => 'number',
																'title'=>'Requires 8 characters and all digits',
                                                                'placeholder' => 'e.g. 91112222' ]); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="incidentType" class="col-sm-4 control-label">Incident Type</label>
                            <div class="col-sm-4">
                                <?= $this->Form->input('incident_type_id', [ 'label' => false, 
                                                                            'class' => 'form-control', 
                                                                            'options' => $incidentTypes]); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="location" class="col-sm-4 control-label">Location</label>
                            <div class="col-sm-4">
                                <?= $this->Form->input('location', [ 'label' => false, 
                                                                'class' => 'form-control', 
                                                                'placeholder' => 'e.g. Chua Chu Kang Blk 438' ]); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="incidentDesc" class="col-sm-4 control-label">Incident Description</label>
                            <div class="col-sm-4">

                                <?= $this->Form->input('description', [ 'label' => false, 
                                                                'class' => 'form-control', 
                                                                'placeholder' => 'e.g. Terrible smoke around the area' ]); ?>
                            </div>
                        </div>
                        
                        
                            
                        <?php
                        $this->Form->templates([
                        ]);

                        echo $this->Form->input(
                            'level',
                            [   'type' => 'radio',
                                'label' => false,
                                'templates' => [
                                    // defines the container wrapping around the radio button group
                                    'inputContainer' => '<div class="form-group">
                        <label for="incidentLevel" class="col-sm-4 control-label">Incident Level</label> {{content}} </div>',
                                    // defines how each div wraps around each <label><input /></label>
                                    'nestingLabel' => '{{hidden}}<label{{attrs}}>{{input}} {{text}}</label>',
                                    'radioWrapper' => '<div class="radio col-sm-1">{{label}}</div>'
                                ],
                                'options' => [
                                ['value' => '1', 'text' => '1', 'checked' => 'checked'],
                                ['value' => '2', 'text' => '2'],
                                ['value' => '3', 'text' => '3'],
                                ['value' => '4', 'text' => '4'],
                                ['value' => '5', 'text' => '5'], ]
                            ]
                        ); 
                        ?>
                        
                    </div>
                    
                    
                    
                    <div class="box-footer">
                        <?= $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-default']) ?>
                        <!-- button type="submit" class="btn btn-default">Reset</button -->
                        <?= $this->Form->button('Update', ['type' => 'submit', 'class' => 'btn btn-success pull-right']) ?>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
            
        <div class="clearfix visible-sm-block"></div>
    </div>
	
</section>

<script>

$.validator.addMethod(
	"regex",
	function(value, element, regexp) {
		var check = false;
		return this.optional(element) || regexp.test(value);
	},
	"This field consists of invalid characters."
);


$( "#updateForm" ).validate({
          rules: {
			reporter_name:{
				required: true,
				regex: /^[a-zA-Z][a-zA-Z_\s]+$/
			},
            reporter_mobile: {
              required: true,
			  digits: true,
              minlength: 8,
              maxlength: 8
            }
          }
        });

</script>

<!-- 
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incident->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incident Types'), ['controller' => 'IncidentTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Incident Type'), ['controller' => 'IncidentTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Call Operators'), ['controller' => 'CallOperators', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Call Operator'), ['controller' => 'CallOperators', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incidents form large-9 medium-8 columns content">
    <?= $this->Form->create($incident) ?>
    <fieldset>
        <legend><?= __('Edit Incident') ?></legend>
        <?php
            echo $this->Form->input('location');
            echo $this->Form->input('description');
            echo $this->Form->input('status');
            echo $this->Form->input('reporter_name');
            echo $this->Form->input('reporter_mobile');
            echo $this->Form->input('datetime');
            echo $this->Form->input('incident_type_id', ['options' => $incidentTypes]);
            echo $this->Form->input('call_operator_id', ['options' => $callOperators]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
-->