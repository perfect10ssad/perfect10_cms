<?= $this->element('content-header', ['title' => 'List of Incident Records']);
    $this->fetch('content-header'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Reporter</th>
                                <th>Reporter Mobile</th>
                                <th>Level</th>
                                <th>Date/Time</th>
                                <th>Incident Type</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- if there are no incidents, i will print a message -->
                        <?php if ( empty($incidents) ) :
                                echo "There are no incident records.";
                              else :
                                foreach ($incidents as $incident) : ?>
                                <tr>
                                    <td><?= $this->Number->format($incident->id) ?></td>
                                    <td><?= h($incident->location) ?></td>
                                    <td><?= h($incident->status) ?></td>
                                    <td><?= h($incident->reporter_name) ?></td>
                                    <td><?= h($incident->reporter_mobile) ?></td>
                                    <td><?= h($incident->level) ?></td>
                                    <td><?= h($incident->datetime->setTimeZone(new DateTimeZone('Asia/Singapore'))) ?></td>
                                    <td><?= $incident->has('incident_type') ? $this->Html->link($incident->incident_type->name, ['controller' => 'IncidentTypes', 'action' => 'view', $incident->incident_type->id]) : '' ?></td>
                                    <td>
                                        <?php echo $this->Form->button('Update', [ 'type' => 'submit', 
                                                                            'class' => 'btn btn-warning', 
                                                                            'onclick' => 'window.location.href=\''. $this->Url->build(['action' => 'edit', $incident->id ]) .'\';']);
                                            echo '&nbsp';
                                            if ( strcmp($incident->status, "Ongoing") == 0 ) {
                                                echo $this->Form->postLink(__('Resolve'), ['action' => 'resolve', $incident->id], ['class'=>'btn btn-success', 'confirm' => __('Are you sure this incident {0} has been resolved?', $incident->id)]);
                                            } ?>
                                    </td>
                                </tr>
                        <?php   endforeach;
                              endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Reporter</th>
                                <th>Reporter Mobile</th>
                                <th>Level</th>
                                <th>Date/Time</th>
                                <th>Incident Type</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <script>
        $(function () {
          $("#example1").DataTable({
            "order":[[6, 'desc']]
          });
        });
    </script>
</section>
