<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
?>
<div class="alert alert-info text-center" onclick="this.classList.add('hidden')" role="alert"><strong><?= h($message) ?></strong></div>