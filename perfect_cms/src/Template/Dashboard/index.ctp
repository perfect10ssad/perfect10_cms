                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Version 1.0</small>
                    </h1>
                    
          <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
        <section class="content">
                    <div class="row">
                        <div class="col-md-7">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <?php foreach ($incidentTypes as $i => $incidentType): ?>
                    <!-- make the first tab active -->
                    <li <?php if ($i == 0) echo 'class="active"' ?>>
                      <a href="#<?= str_replace(' ', '_', $incidentType->name) ?>" data-toggle="tab"><?= $incidentType->name?>
                    </a>
                    </li>
                  <?php endforeach; ?>
                </ul>
                <div class="tab-content">
                  <!-- for each incident type -->
                  <?php foreach ($incidentTypes as $i => $incidentType): ?>

                  <div class="<?php if ($i == 0) echo 'active' ?> tab-pane" id="<?= str_replace(' ', '_', $incidentType->name) ?>">
                    <!-- Post -->
                    <?php 
                    if (count($incidentType->incidents) > 0) {
                    foreach ($incidentType->incidents as $incident): ?>
                      <div class="post">
                        <div class="user-block">
                          <?php $imgFile = 'img/user1.png';

                                if($incident->call_operator->role == 'admin')
                                    $imgFile = 'img/admin.png';?>
                        <img class="img-circle img-bordered-sm" src=<?= $imgFile ?> alt="user image">
                          <span class="username">
                            <a href="#"><?= $incident->call_operator->name ?></a>
                          </span>
                        <span class="description">
                                <?= $this->Time->format(
                                  $incident->datetime,
                                  \IntlDateFormatter::FULL,
                                  null
                                ); ?>
                        </span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                        <?= $incident->description ?>
                        </p>

                        
                      </div>
                    <?php endforeach;
                    } else { ?>
                      <div class="post">
                        No posts for this category yet.
                      </div>
                    <?php } ?>
                  <!-- /.post -->

                  
                  </div>
                  <!-- /.tab-pane -->
                  <?php endforeach; ?>
                  
                </div>
                <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <div class="col-md-5">
              <!-- AREA CHART -->
              <div class="box box-success">
                <div class="box-header with-border">
                <h3 class="box-title">
                  Hourly 24-hour PSI
                </h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                  <p>Results based off National Reporting Stations</p>
                  <canvas id="areaChart" style="height:250px"></canvas>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
                    </div>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="clearfix visible-sm-block"></div>
                    </div>
                </section>

<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      // take last 10 timings and add them as labels
      labels: <?= json_encode($nrs_timings) ?>,
      datasets: [
        {
          label: "PSI",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          // take last 10 PSI values
          data: <?= json_encode($nrs_psi_readings) ?>
        },
        {
          label: "PM2.5",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($nrs_pm25_readings) ?>
        }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

      // This will make tooltip have it's label prepended to it
      multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart = areaChart.Line(areaChartData, areaChartOptions);
    });
</script>