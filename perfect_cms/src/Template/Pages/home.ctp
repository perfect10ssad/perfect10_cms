                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Version 1.0</small>
                    </h1>
                    
          <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
        <section class="content">
                    <div class="row">
                        <div class="col-md-7">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fire" data-toggle="tab">Fire</a></li>
                  <li><a href="#haze" data-toggle="tab">Haze</a></li>
                </ul>
                <div class="tab-content">
                  
                  <div class="active tab-pane" id="fire">
                    <!-- Post -->
                    <div class="post">
                      <div class="user-block">
                      <img class="img-circle img-bordered-sm" src="img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#">Call Operator 1 (can put a random pic)</a>
                        </span>
                      <span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
                      </div>
                      <!-- /.user-block -->
                      <p>
                      Lorem ipsum represents a long-held tradition for designers,
                      typographers and the like. Some people hate it and argue for
                      its demise, but others ignore the hate as they create awesome
                      tools to help create filler text for everyone from bacon lovers
                      to Charlie Sheen fans.
                      </p>
                      <img class="img-responsive"  src="img/fire1.jpg" alt="user image">

                      
                    </div>
                  <!-- /.post -->
                  
                  <!-- Post -->
                  <div class="post">
                    <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="img/user1-128x128.jpg" alt="user image">
                      <span class="username">
                        <a href="#">Call Operator 2 (can put a random pic)</a>
                      </span>
                    <span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
                    </div>
                    <!-- /.user-block -->
                    <p>
                    Lorem ipsum represents a long-held tradition for designers,
                    typographers and the like. Some people hate it and argue for
                    its demise, but others ignore the hate as they create awesome
                    tools to help create filler text for everyone from bacon lovers
                    to Charlie Sheen fans.
                    </p>
                    <img class="img-responsive"  src="img/fire2.jpg" alt="user image">

                  </div>

                  
                  </div>
                  <!-- /.tab-pane -->
                  
                  
                  <div class="tab-pane" id="haze">
                  <!-- Post -->
                  <div class="post">
                    <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="img/user2-160x160.jpg" alt="user image">
                      <span class="username">
                        <a href="#">Call Operator 1 (can put a random pic)</a>
                      </span>
                    <span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
                    </div>
                    <!-- /.user-block -->
                    <p>
                    Lorem ipsum represents a long-held tradition for designers,
                    typographers and the like. Some people hate it and argue for
                    its demise, but others ignore the hate as they create awesome
                    tools to help create filler text for everyone from bacon lovers
                    to Charlie Sheen fans.
                    </p>
                    <img class="img-responsive"  src="img/haze1.jpg" alt="user image">

                    
                  </div>
                  <!-- /.post -->

                  <!-- Post -->
                  <div class="post">
                    <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="img/user3-128x128.jpg" alt="user image">
                      <span class="username">
                        <a href="#">Call Operator 2 (can put a random pic)</a>
                      </span>
                    <span class="description">(get the time auto from system shou xian) 7:30 PM today</span>
                    </div>
                    <!-- /.user-block -->
                    <p>
                    Lorem ipsum represents a long-held tradition for designers,
                    typographers and the like. Some people hate it and argue for
                    its demise, but others ignore the hate as they create awesome
                    tools to help create filler text for everyone from bacon lovers
                    to Charlie Sheen fans.
                    </p>
                    <img class="img-responsive"  src="img/haze2.jpg" alt="user image">

                  </div>

                  
                  </div>
                  <!-- /.tab-pane -->
                  
                  
                  
                </div>
                <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <div class="col-md-5">
              <!-- AREA CHART -->
              <div class="box box-success">
                <div class="box-header with-border">
                <h3 class="box-title">
                  <?php echo is_array($nea->channel) ?>
                </h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                  <canvas id="areaChart" style="height:250px"></canvas>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
                    </div>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="clearfix visible-sm-block"></div>
                    </div>
                </section>

<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "Electronics",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: "Digital Goods",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);
    });
</script>