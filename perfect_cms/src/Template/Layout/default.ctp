<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Crisis Management System';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- Meha's template CSS here -->
    <?= $this->Html->css([ "bootstrap.min.css", 
                        "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css", 
                        "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css", 
                        "/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                        "AdminLTE.min.css",
                        "skin-black.min.css",
                        "fontawesome-iconpicker.css" ]) ?>

    <?= $this->Html->script(["/plugins/jQuery/jQuery-2.2.0.min.js",
                            "/plugins/sparkline/jquery.sparkline.min.js",
                            "/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                            "/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                            "/plugins/slimScroll/jquery.slimscroll.min.js",
                            "/plugins/chartjs/Chart.min.js",
                            "/plugins/datatables/jquery.dataTables.min.js",
                            "/plugins/datatables/dataTables.bootstrap.min.js",
                            "/plugins/fastclick/fastclick.js",
                            "bootstrap.min.js",
                            "app.min.js",
                            "http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.js",
                            //"pages/dashboard2.js",
                            "demo.js",
                            "fontawesome-iconpicker.js" 
                            ]) ?>
</head>
<body class="hold-transition skin-black sidebar-mini">
    
    <div class="wrapper">
       <!-- ****************************************START OF HEADER & SIDEBAR*********************************************-->
       <header class="main-header">
            <?= $this->Html->link(
                '<span class="logo-mini"><b>CMS</b></span>
                <span class="logo-lg"><b>C</b>risis <b>M</b>gmt <b>S</b>ystem</span>',
                "/",
                [ 'class' => 'logo', 'escape' => false]
            ); ?>
            
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success">1</span>
                            </a>
                            
                            <ul class="dropdown-menu">
                                <li class="header">1 notification(s)</li>
                                <li>
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <?= $this->Html->image('admin.png', ['class' => 'img-circle']) ?>
                                                </div>
                                                <h4>
                                                    Category added:
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>A new category <b>Test Category</b> has <br/>been added by the administrator</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        
                        <!-- top right corner photos -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php 
                                    $imgFile = 'user1.png';

                                    if($this->request->session()->read('Auth.User.role') == 'admin')
                                        $imgFile = 'admin.png';

                                    echo $this->Html->image($imgFile, ['class' => 'user-image']) ?>
                                <span class="hidden-xs"><?= $this->request->session()->read('Auth.User.name') ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <?php

                                        echo $this->Html->image($imgFile, ['class' => 'img-circle']) ?>
                                    <p><?= $this->request->session()->read('Auth.User.name') ?></p>
                                </li>
                                
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?= $this->Html->link('Lock', // text of the link
                                                                ['controller' => 'CallOperators', 'action' => 'lock'], // application specific
                                                                ['class'=> 'btn btn-default btn-flat'] // html specific
                                                                ) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= $this->Html->link('Sign out', // text of the link
                                                                ['controller' => 'CallOperators', 'action' => 'logout'], // application specific
                                                                ['class'=> 'btn btn-default btn-flat'] // html specific
                                                                ) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <?= $this->Html->image($imgFile, ['class' => 'img-circle']) ?>
                    </div>
                    <div class="pull-left info">
                        <!-- user name -->
                        <p><?= $this->request->session()->read('Auth.User.name') ?>
                        </p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    
                    <li>
                        <?= $this->Html->link(
                            '<i class="fa fa-dashboard"></i><span>Dashboard</span>',
                            ['controller' => 'Dashboard', 'action' => 'index', '_full' => true],
                            ['escapeTitle' => false]
                        ); ?>
                    </li>
                    
                   <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-edit"></i> <span>Incidents</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="active">
                                <?= $this->Html->link(
                                    '<i class="fa fa-circle-o"></i>Create New Incident',
                                    ['controller' => 'Incidents', 'action' => 'add', '_full' => true],
                                    ['escapeTitle' => false] // escape needs to be here to load the <i class> tag
                                ); ?>
                            </li>
                            <li class="active">
                                <?= $this->Html->link(
                                    '<i class="fa fa-circle-o"></i>Incident Records',
                                    ['controller' => 'Incidents', 'action' => 'index', '_full' => true],
                                    ['escapeTitle' => false]
                                ); ?>
                            </li>
                        </ul>
                    </li>
                    <?php if( strcmp($role, 'admin') == 0 ) { ?> 
                            <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-heart"></i> <span>Incident Types</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                            <ul class="treeview-menu">
                                <li class="active">
                                    <?= $this->Html->link(
                                    '<i class="fa fa-circle-o"></i>Create New Incident Type',
                                    ['controller' => 'IncidentTypes', 'action' => 'add', '_full' => true],
                                    ['escapeTitle' => false] // escape needs to be here to load the <i class> tag
                                ); ?>
                                </li>
                                <li class="active">
                                    <?= $this->Html->link(
                                       '<i class="fa fa-circle-o"></i>List of Incident Types',
                                        ['controller' => 'IncidentTypes', 'action' => 'index', '_full' => true],
                                        ['escapeTitle' => false]
                                    ); ?>
                                </li>
                            </ul>
                        </li>

                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-heart"></i> <span>Assistance Types</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                            <ul class="treeview-menu">
                                <li class="active">
                                    <?= $this->Html->link(
                                    '<i class="fa fa-circle-o"></i>Create New Assistance Type',
                                    ['controller' => 'AssistanceTypes', 'action' => 'add', '_full' => true],
                                    ['escapeTitle' => false] // escape needs to be here to load the <i class> tag
                                ); ?>
                                </li>
                                <li class="active">
                                    <?= $this->Html->link(
                                       '<i class="fa fa-circle-o"></i>List of Assistance Types',
                                        ['controller' => 'AssistanceTypes', 'action' => 'index', '_full' => true],
                                        ['escapeTitle' => false]
                                    ); ?>
                                </li>
                            </ul>
                        </li>

                         <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-heart"></i> <span>Agency</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                            <ul class="treeview-menu">
                                <li class="active">
                                    <?= $this->Html->link(
                                    '<i class="fa fa-circle-o"></i>Create New Agency',
                                    ['controller' => 'Agencies', 'action' => 'add', '_full' => true],
                                    ['escapeTitle' => false] // escape needs to be here to load the <i class> tag
                                ); ?>
                                </li>
                                
                                <li class="active">
                                    <?= $this->Html->link(
                                       '<i class="fa fa-circle-o"></i>List of Agencies',
                                        ['controller' => 'Agencies', 'action' => 'index', '_full' => true],
                                        ['escapeTitle' => false]
                                    ); ?>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </section>
        </aside>

            <!-- End of header and sidebar, note that sidebar should be dynamic -->
    <div class="content-wrapper">
        <!-- im sorry fellow css professionals, i just couldn't give a damn -->
        <div style="padding: 20px">
            <?= $this->Flash->render() ?>
        </div>
        <?= $this->fetch('content') ?>
    </div>

    <!-- Footer here -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Perfect10</strong>
    </footer>

    <div class="control-sidebar-bg"></div>
</body>
</html>
