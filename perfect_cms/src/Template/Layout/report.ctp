<!-- We only require some of the js and css files in this template to generate a report template. -->

<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>
        <?= 'CMS Report' ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- Meha's template CSS here -->
    <?= $this->Html->css([ "bootstrap.min.css", 
                        "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"]) ?>

    <?= $this->Html->script(["/plugins/jQuery/jQuery-2.2.0.min.js",
                            "/plugins/chartjs/Chart.min.js",
                            "bootstrap.min.js",
                            "https://maps.googleapis.com/maps/api/js?key=AIzaSyAt5qhOyrOsbtBeXhuNULgQnsUeMMsCKVQ
        &library=places",
        "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js"
                            ]) ?>
</head>
<body>
<?= $this->fetch('content') ?>
</body>
</html>