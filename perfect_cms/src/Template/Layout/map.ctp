<?php

$cakeDescription = 'Perfect10 Incident Map';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- Meha's template CSS here -->
    <?= $this->Html->css([ "bootstrap.min.css", 
                        "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css", 
                        "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css", 
                        "AdminLTE.min.css",
                        "skin-green-light.min.css" ]) ?>

    <?= $this->Html->script(["/plugins/jQuery/jQuery-2.2.0.min.js",
                            "/plugins/sparkline/jquery.sparkline.min.js",
                            "/plugins/slimScroll/jquery.slimscroll.min.js",
                            "/plugins/chartjs/Chart.min.js",
                            "/plugins/datatables/jquery.dataTables.min.js",
                            "/plugins/datatables/dataTables.bootstrap.min.js",
                            "/plugins/fastclick/fastclick.js",
                            "bootstrap.min.js",
                            "app.min.js",
                            "http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.js",
                            //"pages/dashboard2.js",
                            "demo.js"
                            ]) ?>
</head>
<body class="hold-transition skin-green-light sidebar-mini sidebar-collapse">
    
    <div class="wrapper">
       <!-- ****************************************START OF HEADER & SIDEBAR*********************************************-->
       <header class="main-header">
            <?= $this->Html->link(
                '<span class="logo-mini"><b>P10</b></span>
                <span class="logo-lg"><b>P</b>erfect <b>10</b></span>',
                "/map",
                [ 'class' => 'logo', 'escape' => false]
            ); ?>
            
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
                </a>
            </nav>

        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                
                <ul class="sidebar-menu">
                    <li class="header">Map Categories</li>
                        <li>
                            <a href="#" onclick="retrieveHazeMarkers();">
                                <i class="fa fa-cloud fa-blue"></i><span>PSI Readings</span>
                            </a>
                    <?php foreach($incidentTypes as $incidentType): ?>
	                    <li>
                            <a href="#" onclick="filterMarkers('<?= $incidentType->name ?>')">
                                <i class="fa <?= $incidentType->icon ?> <?= $incidentType->icon_color ?>"></i><span><?= $incidentType->name ?></span>
                            </a>
						</li>
					<?php endforeach;?>
                    
                    <li class="header">Stay updated</li>
                    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-forumbee text-red"></i> <span>Subscribe!</span></a></li>
                    
                </ul>
            </section>
        </aside>

            <!-- End of header and sidebar, note that sidebar should be dynamic -->
    <div class="content-wrapper">

        <!-- //$this->Flash->render() -->
        <?= $this->fetch('content') ?>
    </div>

    <!-- Footer here -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Perfect10</strong>
    </footer>

    <div class="control-sidebar-bg"></div>
</body>
</html>
