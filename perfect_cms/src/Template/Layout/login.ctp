<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- Meha's template CSS here -->
    <?= $this->Html->css([ "bootstrap.min.css", 
                        "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css", 
                        "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css", 
                        "/plugins/iCheck/square/blue.css",
                        "AdminLTE.min.css",
                        "skin-black.min.css" ]) ?>
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>Crisis </b> Mgmt System</a>
        </div>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
        
    </div>

    <?= $this->Html->script(['/plugins/jQuery/jQuery-2.2.0.min.js', '/plugins/iCheck/icheck.min.js', 'bootstrap.min.js']) ?>
    <script>
        $(function () {
          $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
          });
        });
    </script>
</body>
</html>