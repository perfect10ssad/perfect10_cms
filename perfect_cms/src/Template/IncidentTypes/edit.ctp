<?= $this->element('content-header', ['title' => 'Edit Incident Type']);
    $this->fetch('content-header');
    ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                
                <div class="box-body">
                <?= $this->Form->create($incidentType, ['id'=>'editForm']) ?>

                    <div class="form-group">
                        <?= $this->Form->input('name', ['class' => 'form-control']); ?>
                    </div>

                     <div class="form-group">
                        <?= $this->Form->input('assistance_types._ids', ['options' => $assistanceTypes, 'class' => 'form-control']); ?>
                    </div>

                    <div class="form-group">
                        <label>Icon</label><br/>
                        <?= $this->Form->input('icon', [ 'type' => 'hidden', 
                                                        'data-placement' => 'inline', 
                                                        'class' => 'form-control icp-auto',
                                                        'value' => $incidentType->icon]); ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            $colors = array('text-yellow', 'text-aqua', 'text-blue', 'text-black', 'text-light-blue', 'text-green', 'text-gray', 'text-navy', 'text-teal', 'text-olive', 'text-lime', 'text-orange', 'text-fuchsia', 'text-purple', 'text-maroon');

                            $colors = array_combine($colors, $colors);

                            echo $this->Form->input('icon_color', ['options' => $colors, 'class' => 'form-control']);
                        ?>
                    </div>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
            
        <div class="clearfix visible-sm-block"></div>
    </div>
</section>
<div class="incidentTypes form large-9 medium-8 columns content">

</div>
<script>
// this enables the icon picker to appear
$('.icp-auto').iconpicker();

$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "This field consists of invalid characters."
);

$( "#editForm" ).validate({
          rules: {
            name: {
              required: true,
			  regex: /^[a-zA-Z][a-zA-Z\d\-_\s]+$/
            }
          }
        });


</script>