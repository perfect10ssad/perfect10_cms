<div class="incidentTypes index large-9 medium-8 columns content">
    <h3><?= __('Incident Types') ?></h3>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="index_table" class="table table-bordered table-striped" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Assistance Types</th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($incidentTypes as $incidentType): ?>
                    <tr>
                        <td><?= $this->Number->format($incidentType->id) ?></td>
                        <td><?= h($incidentType->name) ?></td>
                        <td>
                        <?php foreach ($incidentType->assistance_types as $assistanceTypes): ?>
                            <?= h($assistanceTypes->name) ?><br/>
                        <?php endforeach; ?>
                        </td>
                        <td class="actions">
                            <?= $this->Html->link(__('Update'), ['action' => 'edit', $incidentType->id], ['class'=>'btn btn-warning']) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $incidentType->id], ['class'=>'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $incidentType->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

    <script>
        $(function () {
          $("#index_table").DataTable();
        });
    </script>
