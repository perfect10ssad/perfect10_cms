<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Incident Type'), ['action' => 'edit', $incidentType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Incident Type'), ['action' => 'delete', $incidentType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incidentType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Incident Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident'), ['controller' => 'Incidents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assistance Types'), ['controller' => 'AssistanceTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assistance Type'), ['controller' => 'AssistanceTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="incidentTypes view large-9 medium-8 columns content">
    <h3><?= h($incidentType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($incidentType->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($incidentType->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Incidents') ?></h4>
        <?php if (!empty($incidentType->incidents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Location') ?></th>
                <th><?= __('Location Lat') ?></th>
                <th><?= __('Location Lng') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Reporter Name') ?></th>
                <th><?= __('Reporter Mobile') ?></th>
                <th><?= __('Datetime') ?></th>
                <th><?= __('Incident Type Id') ?></th>
                <th><?= __('Call Operator Id') ?></th>
                <th><?= __('Level') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incidentType->incidents as $incidents): ?>
            <tr>
                <td><?= h($incidents->id) ?></td>
                <td><?= h($incidents->name) ?></td>
                <td><?= h($incidents->location) ?></td>
                <td><?= h($incidents->location_lat) ?></td>
                <td><?= h($incidents->location_lng) ?></td>
                <td><?= h($incidents->description) ?></td>
                <td><?= h($incidents->status) ?></td>
                <td><?= h($incidents->reporter_name) ?></td>
                <td><?= h($incidents->reporter_mobile) ?></td>
                <td><?= h($incidents->datetime) ?></td>
                <td><?= h($incidents->incident_type_id) ?></td>
                <td><?= h($incidents->call_operator_id) ?></td>
                <td><?= h($incidents->level) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Incidents', 'action' => 'view', $incidents->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Incidents', 'action' => 'edit', $incidents->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Incidents', 'action' => 'delete', $incidents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incidents->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Assistance Types') ?></h4>
        <?php if (!empty($incidentType->assistance_types)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Number') ?></th>
                <th><?= __('Agency Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incidentType->assistance_types as $assistanceTypes): ?>
            <tr>
                <td><?= h($assistanceTypes->id) ?></td>
                <td><?= h($assistanceTypes->name) ?></td>
                <td><?= h($assistanceTypes->number) ?></td>
                <td><?= h($assistanceTypes->agency_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AssistanceTypes', 'action' => 'view', $assistanceTypes->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'AssistanceTypes', 'action' => 'edit', $assistanceTypes->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AssistanceTypes', 'action' => 'delete', $assistanceTypes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assistanceTypes->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
