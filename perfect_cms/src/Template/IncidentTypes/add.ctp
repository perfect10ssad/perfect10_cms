<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Incident Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Incident'), ['controller' => 'Incidents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assistance Types'), ['controller' => 'AssistanceTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assistance Type'), ['controller' => 'AssistanceTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incidentTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($incidentType) ?>
    <fieldset>
        <legend><?= __('Add Incident Type') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('assistance_types._ids', ['options' => $assistanceTypes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
