<style>
.chart_legend span{
    display: inline-block;
    width: 12px;
    height: 12px;
    margin-right: 5px;
}
</style>
<div class="container">
  <div class="jumbotron">
    <h2>Half-Hourly Incident Report</h2>
    <p>Information provided from P10 Crisis Management System</p>
  </div>
  <div class="row">
    <div class="col-sm-12 table-responsive">
      <h3>Past 10 records of 24 hour PSI</h3>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Time</th>
            <th>PSI</th>
            <th>PM2.5</th>
          </tr>
        </thead>
        <tbody>
          <?php for($i = 0; $i < 10; $i++) {?>
          <tr>
            <td><?= $nrs_timings[$i] ?></td>
            <td><?= $nrs_psi_readings[$i] ?></td>
            <td><?= $nrs_pm25_readings[$i] ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h3>Past 10 readings across 5 regions of Singapore</h3>
        <div class="col-sm-6" style="width:340px; float:left">
          <center><h4>PSI</h4></center>
          <canvas id="areaChart_PSI" style="height:300px"></canvas>
        </div>
        <div class="col-sm-6" style="width:340px; float:right">
          <center><h4>PM2.5</h4></center>
          <canvas id="areaChart_PM25" style="height:300px"></canvas>
        </div>
    </div>
  </div>
  <center><div class="chart_legend" id="chart_legend"></div></center>
  <div class="row">
    <div class="col-sm-12">
      <h3>
      Past 30 minute incident reports
      </h3>
      <?php 

      $options = array('size'=>'700x400', 'scale'=>'2', 'format'=>'png', 'zoom'=>'11','center'=>'Singapore');
      $map = new StaticMap($options);

      foreach($incidents as $incident) {
        $styles = array('label'=> substr($incident->incident_type->name, 0, 1), 'color'=>substr($incident->incident_type->icon_color, 5));
        $map->add_marker($incident->location, $styles);
      }

      // Display Static Map (using magic methods)
      echo "<center><img src=\"$map\" alt=\"Google Map\" $map->dimensions></center>"; ?>
    </div>
  </div>
    <div class="row">
      <div class="col-sm-12 table-responsive">
      <h3>Summary records of all Incidents</h3>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Incident Type</th>
            <th>Ongoing</th>
            <th>Completed</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($incidentTypes as $incidentType) {?>
          <tr>
            <td><?= $incidentType->name ?></td>
            <td><?php 
                    $count = 0;
                    foreach($incidents as $incident) {
                      if( strcmp($incident->status, 'Ongoing') == 0 && strcmp($incident->incident_type->name, $incidentType->name) == 0 ) {
                        $count++;
                      }
                    } 
                    echo $count; ?></td>
            <td><?php
                    $count = 0;
                    foreach($incidents as $incident) {
                      if( strcmp($incident->status, 'Completed') == 0 && strcmp($incident->incident_type->name, $incidentType->name) == 0 ) {
                        $count++;
                      }
                    } 
                    echo $count; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>


<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas_PSI = $("#areaChart_PSI").get(0).getContext("2d");
    var areaChartCanvas_PM25 = $("#areaChart_PM25").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart_PSI = new Chart(areaChartCanvas_PSI);
    var areaChart_PM25 = new Chart(areaChartCanvas_PM25);

    var centralColor = "rgba(204, 255, 102, 1)";
    var northColor = "rgba(255, 255, 153, 1)";
    var westColor = "rgba(102,0,255,1)";
    var eastColor = "rgba(0,204,153,1)";
    var southColor = "rgba(255,102,153,1)";

    var areaChartData = {
      // take last 10 timings and add them as labels
      labels: <?= json_encode($nrs_timings) ?>,
      datasets: [
        {
          label: "Central",
          fillColor: "rgba(204, 255, 102, 0)",
          strokeColor: centralColor,
          pointColor: centralColor,
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          // take last 10 PSI values
          data: <?= json_encode($chart_psi['ce_psi']) ?>
        },
        {
          label: "North",
          fillColor: "rgba(255, 255, 102, 0)",
          strokeColor: northColor,
          pointColor: northColor,
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          // take last 10 PSI values
          data: <?= json_encode($chart_psi['no_psi']) ?>
        },
        {
          label: "West",
          fillColor: "rgba(255,204,153,0)",
          strokeColor: westColor,
          pointColor: westColor,
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($chart_psi['we_psi']) ?>
        },
        {
          label: "East",
          fillColor: "rgba(0,204,153,0)",
          strokeColor: eastColor,
          pointColor: eastColor,
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($chart_psi['ea_psi']) ?>
        },
        {
          label: "South",
          fillColor: "rgba(255,102,153,0)",
          strokeColor: southColor,
          pointColor: southColor,
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($chart_psi['so_psi']) ?>
        }
      ]
    };

    var areaChartData_PM25 = {
      // take last 10 timings and add them as labels
      labels: <?= json_encode($nrs_timings) ?>,
      datasets: [
        {
          label: "Central",
          fillColor: "rgba(204, 255, 102, 0)",
          strokeColor: centralColor,
          pointColor: centralColor,
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          // take last 10 PSI values
          data: <?= json_encode($chart_pm25['ce_pm25']) ?>
        },
        {
          label: "North",
          fillColor: "rgba(255, 255, 102, 0)",
          strokeColor: northColor,
          pointColor: northColor,
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          // take last 10 PSI values
          data: <?= json_encode($chart_pm25['no_pm25']) ?>
        },
        {
          label: "West",
          fillColor: "rgba(255,204,153,0)",
          strokeColor: westColor,
          pointColor: westColor,
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($chart_pm25['we_pm25']) ?>
        },
        {
          label: "East",
          fillColor: "rgba(0,204,153,0)",
          strokeColor: eastColor,
          pointColor: eastColor,
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($chart_pm25['ea_pm25']) ?>
        },
        {
          label: "South",
          fillColor: "rgba(255,102,153,0)",
          strokeColor: southColor,
          pointColor: southColor,
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          // take last 10 PM2.5 values
          data: <?= json_encode($chart_pm25['so_pm25']) ?>
        }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<% for (var i=0; i<datasets.length; i++){%><span style=\"background-color:<%=datasets[i].pointColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%> <%}%>",

      // This will make tooltip have it's label prepended to it
      multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart_PSI = areaChart_PSI.Line(areaChartData, areaChartOptions);
    areaChart_PM25 = areaChart_PM25.Line(areaChartData_PM25, areaChartOptions);
    document.getElementById('chart_legend').innerHTML = areaChart_PM25.generateLegend();
    });
</script>