<?= $this->element('content-header', ['title' => 'Create New Agency']);
    $this->fetch('content-header'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Create New Agency</h3>
                </div>
                
                <?= $this->Form->create($agency, ['class' => 'form-horizontal', 'id'=>'addForm']) ?>
                <!-- <form class="form-horizontal">-->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="agency" class="col-sm-2 control-label">Agency Name : </label>
                            <div class="col-sm-8">
                                <?= $this->Form->text('name', ['class' => 'form-control', 'placeholder' => 'Enter an agency name']) ?>
                                <!-- input type="email" class="form-control" id="name" placeholder="e.g. Sally" -->
                            </div>
                            <div>
                      
                            </div>
                        </div>
						
                    </div>
                    
                    <div class="box-footer">
                        <?= $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-default']) ?>
                        <!-- button type="submit" class="btn btn-default">Reset</button -->
                        <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success pull-right']) ?>
                        <!-- button type="submit" class="btn btn-success pull-right">Submit</button -->
                    </div>
                <?= $this->Form->end() ?>
                <!-- /form -->
            </div>
        </div>
            
        <div class="clearfix visible-sm-block"></div>
    </div>
</section>

<script>

$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "This field consists of invalid characters."
);

$( "#addForm" ).validate({
          rules: {
            name: {
              required: true,
			  regex: /^[a-zA-Z][a-zA-Z\d\-_\s]+$/
            }
          }
        });
</script>