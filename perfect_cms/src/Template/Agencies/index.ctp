<?= $this->element('content-header', ['title' => 'List of Agencies Records']);
    $this->fetch('content-header'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Agency Name</th>     
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- if there are no assistance type, i will print a message -->
                        <?php if ( empty($Agencies) ) :
                                echo "There are no agency records.";
                              else :
                                foreach ($Agencies as $agency) : ?>
                                <tr>
                                    <td><?= $agency->name ?></td>
                                  
                                
                                    <td>
                                        <?= $this->Form->button('Update', [ 'type' => 'submit', 
                                                                            'class' => 'btn btn-warning', 
                                                                            'onclick' => 'window.location.href=\''. $this->Url->build(['action' => 'edit', $agency->id ]) .'\';']) ?>
										<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $agency->id], ['class'=>'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $agency->id)]) ?>
                                    </td>
                                </tr>
                        <?php   endforeach;
                              endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Agency Name</th>   
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <script>
        $(function () {
          $("#example1").DataTable();
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
          });
        });
    </script>
</section>


