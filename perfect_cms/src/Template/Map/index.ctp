<section>
<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<?= $this->Form->create($subscriber, ['id'=>'subscribeForm']) ?>
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h2 class="modal-title" id="myModalLabel">Subscribe to Incidents</h2>
			  </div>
			  <div class="modal-body">
				<p>
				Do you want to keep updated to all incidents? You can subscribe to our subscription service to stay informed for any new incidents that might be happening in your area.
				</p>
				Select your preference:
				
					<div class="box-body">
						<div class="col-sm-2 form-group checkbox">
							<label class="control-label">
								<input class="input_control" value="mobile_number" type="checkbox">SMS</label>
						</div>
						<div class="col-sm-6">
							 <?= $this->Form->input('mobile_number', ['pattern'=>".{8,}", 'title'=>'Please enter 8 characters', 'type' => 'number', 'label' => false, 'disabled' => 'disabled',  'placeholder' => 'Enter phone number', 'class' => 'form-control']); ?>
						</div>
					</div>
					<div class="box-body">
						<div class="col-sm-2 form-group checkbox">
							<label class="control-label">
								<input class="input_control" value="email" type="checkbox">Email</label>
							
						</div>
						<div class="col-sm-6">
							<?= $this->Form->input('email', [ 'type' => 'email', 'label' => false, 'disabled' => 'disabled', 'placeholder' => 'Enter email address', 'class' => 'form-control']); ?>
						</div>
					</div>
			  </div>
			  <div class="modal-footer">
				<?= $this->Form->button('Subscribe', ['type' => 'submit', 'disabled' => true, 'id' => 'subscribeBtn','class' => 'btn btn-primary']) ?>
			  </div>
			</div>
		<?= $this->Form->end() ?>
	  </div>
	</div>
</section>
<div id="map" style="height:700px;"></div>

<?php
  // Override any of the following default options to customize your map
  $map_options = array(
	'id' => 'map',
	'zoom' => 12,
	'height' => '87%',
	'width' => '100%',
	'marker' => false,
	'type' => 'ROADMAP',
	'localize' => false, // disable map from getting current position from browser

	// These coordinates will centalize the map to singapore, please do not change it
	'latitude' => 1.352083,
	'longitude' => 103.819836
  );

  /*echo $this->GoogleMap->map($map_options); 
		
		$index = 1;
		foreach ($incidents as $incident):
			$marker_options = array(
				'showWindow' => true,
				"category" => $incident->incident_type->name,
				'windowText' => '<h2>'. $incident->incident_type->name .'</h2><br/><b>Location</b>: ' . $incident->location . '<br/><p>' . $incident->description . '</p>',
				'markerTitle' => $incident->incident_type->name
			);

			echo $this->GoogleMap->addMarker("googleMap", $incident->id, array("latitude" => $incident->location_lat, "longitude" => $incident->location_lng), $marker_options);
			$index++;
		endforeach;*/
?>
<script>
	var map;
	var heatmapData = [];
	function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: 1.352083, lng: 103.819836},
		  zoom: 12
		});
	}

	var incidentObjects;
	var currentMarkers = [];
	var currentIDs = [];
	var newIDs = [];
	var timer = true;

	// Sets the map on all markers in the array.
	function setMapOnAll(map) {
	  for (var i = 0; i < currentMarkers.length; i++) {
	    currentMarkers[i].setMap(map);
	  }
	}

	function retrieveHazeMarkers(){
		// remove other markers and add PSI markers with infowindow opened by default
		setMapOnAll(null);
		currentMarkers = [];
		currentIDs = [];
		newIDs = [];
		clearInterval(incidentJSONretriever);
		timer = false;

		var neaAPI = 'http://www.nea.gov.sg/api/WebAPI/?dataset=psi_update&keyref=781CF461BB6606AD62B1E1CAA87ECA61F2450EE51DF0DC33';
		$.get(neaAPI, {format: 'xml'})
		.done(function(data) {
			console.log(data);
			$(data).find('region').each(function(){
				var lat = $(this).find('latitude').text();
				var lng = $(this).find('longitude').text();
				if(lat != 0) {
					var latLng = new google.maps.LatLng(lat, lng);
					var marker = new google.maps.Marker({
			            position: latLng,
			            map: map,
			            animation: google.maps.Animation.DROP
			        });

			        var reg;
					switch($(this).find('id').text()) {
						case "rNO":
							reg = "North";
							break;
						case "rCE":
							reg = "Central";
							break;
						case "rEA":
							reg = "East";
							break;
						case "rWE":
							reg = "West";
							break;
						case "rSO":
							reg = "South";
							break;
					}

					var contentString = '<h2>PSI: '+$(this).find('reading[type="NPSI"]').attr('value')+'</h2><p>'+ reg +'</p>';

					 var infowindow = new google.maps.InfoWindow({
					   content: contentString
					 });
			        infowindow.open(map, marker);

			        currentMarkers.push(marker);
		    	}
			});
		});
	}

	function createMarker(item){
		var contentString = '<h2>' + item.incident_type.name +'</h2><br/><b>Location</b>: ' + item.location + '<br/><p>' + item.description + '</p>';
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		// Place markers on map
        var latLng = new google.maps.LatLng(item.location_lat, item.location_lng);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            animation: google.maps.Animation.DROP
        });
        
        marker.addListener('click', function() {
		  infowindow.open(map, marker);
		});

		return marker;
	}

	function retrieveIncidents(){
		if(!timer){
			timer = true;
			incidentJSONretriever = setInterval(retrieveIncidents, 60000);
		}

		// this function will retrieve the incidents data from the self created API
	  var incidentsAPI = "incidents/rest.json";
	  $.getJSON( incidentsAPI, {
		format: "json"
	  })
		.done(function( data ) {
			incidentObjects = data.incidents;
			// retrieve new data, compare new data with old data
			// 1) if new data has requestID that is inside of old data, do nothing
			// 2) if new data has requestID that is not inside of old data, add marker
			// 3) if new data does not have a requestID in old data, remove marker
			if(currentIDs.length == 0) {
				// loop for every json object
				$.each(data.incidents, function(i, item) {
					// creates a marker
					marker = createMarker(item);
					currentMarkers.push(marker); // this will save the marker object (for deletion later)
					currentIDs.push(item.id); // appends the id that is currently in the map
				});
			} else {
				newIDs = [];
				var nothingChanged = true;
				// loop for every json object
				$.each(data.incidents, function(i, item) {
					// if new data is not inside of old data
					// add the marker
					if($.inArray(item.id, currentIDs) == -1) {
						console.log('new marker detected!');
						marker = createMarker(item);
						currentMarkers.push(marker);
						currentIDs.push(item.id);
					} else {
						//console.log('item: '+item.id+' is in the currentID array');
					}

					newIDs.push(item.id);
				});

				// loop for all currentRequests on the map
				$.each(currentIDs, function(i, item) {
					// if old data is not inside of new data
					// remove the marker
					if($.inArray(item, newIDs) == -1) {
						console.log('a marker has been removed!');
						marker = currentMarkers[i];
						marker.setMap(null);
						// removes the marker object inside of currentMarkers array
						currentMarkers.splice(i, 1);

						// removes the ID from current ID
						currentIDs.splice(i, 1);
					} else {
						//console.log('item: '+item+' is in the currentID array');
					}
				});
			}
		});

	}

	var incidentJSONretriever;
	retrieveIncidents();
	incidentJSONretriever = setInterval(retrieveIncidents, 60000);

	function toggleVisiblity(incidentType){
		$.each(incidentObjects, function(i, item){
			if(incidentType.localeCompare(item.incident_type.name) != 0) {
				currentMarkers[i].setVisible(false);
			} else {
				currentMarkers[i].setVisible(true);
			}
		});
	}

	function filterMarkers(incidentType) {
		console.log(incidentType);
		if(!timer) {
			retrieveIncidents();
			setMapOnAll(null);
			setTimeout(function() {toggleVisiblity(incidentType);}, 400);
		} else {
			toggleVisiblity(incidentType);
		}
	}

	var checkboxes = $("input[type='checkbox']"),
	submitButt = $("input[type='submit']");

	checkboxes.click(function() {
		$('input[name=' + this.value + ']')[0].required = this.checked;
		$('input[name=' + this.value + ']')[0].disabled = !this.checked;
		$('#subscribeBtn').attr("disabled", !checkboxes.is(":checked"));
	});

	$('#myModal').on('shown.bs.modal', function () {
	  $('#myInput').focus()
	})

	jQuery.validator.setDefaults({
	  success: "You have been subscribed!"
	});

	$( "#subscribeForm" ).validate({
	  rules: {
		mobile_number: {
		  required: true,
		  minlength: 8,
		  maxlength: 8
		}
	  }
	});
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbd_8690nKhYQcJv3wMzbZLiUXziJePkg&libraries=visualization&callback=initMap"
	async defer></script>