<?= $this->Flash->render('auth') ?>
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <?= $this->Form->create() ?>
        <div class="form-group has-feedback">

        	<?= $this->Form->text('username', ['class' => 'form-control', 'placeholder' => 'Username']) ?>
            <!-- input type="email" class="form-control" placeholder="Email" -->
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">

        	<?= $this->Form->password('password', ['class' => 'form-control', 'placeholder' => 'Password']) ?>
            <!-- input type="password" class="form-control" placeholder="Password" -->
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                    <input type="checkbox"> Remember Me
                    </label>
                </div>
            </div>
            
            <div class="col-xs-4">
				<?= $this->Form->button('Login', ['type' => 'submit', 'class' => 'btn btn-primary btn-block btn-flat']); ?>
                <!-- button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button-->
            </div>
            
        </div>
    <?= $this->Form->end() ?>
    <a href="#">I forgot my password</a><br>
</div>

<!-- <div class="callOperators form">
<?= $this->Flash->render('auth') ?>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Please enter your username and password') ?></legend>
        <?= $this->Form->input('username') ?>
        <?= $this->Form->input('password') ?>
    </fieldset>
    <?= $this->Form->button(__('Login')); ?>
    <?= $this->Form->end() ?>
</div>
-->