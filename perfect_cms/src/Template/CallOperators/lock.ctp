<div class="lockscreen-name"><?= $this->request->session()->read('Auth.User.name') ?></div>

<div class="lockscreen-item">
    <div class="lockscreen-image">
        <!-- user image -->
        <?php 
            $imgFile = 'user1.png';

            if($this->request->session()->read('Auth.User.role') == 'admin')
                $imgFile = 'admin.png';

            echo $this->Html->image($imgFile) ?>
    </div>
    <form class="lockscreen-credentials">
        <div class="input-group">
            <input type="password" class="form-control" placeholder="password">
            <div class="input-group-btn">
                <button type="button" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
        </div>
    </form>
</div>

<div class="help-block text-center">
    Enter your password to retrieve your session
</div>

<div class="text-center">
    <a href="login.php"><h4>Sign in as a different user</h4></a>
</div>