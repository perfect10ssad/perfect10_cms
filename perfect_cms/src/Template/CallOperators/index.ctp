<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Call Operator'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Incident'), ['controller' => 'Incidents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="callOperators index large-9 medium-8 columns content">
    <h3><?= __('Call Operators') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('username') ?></th>
                <th><?= $this->Paginator->sort('password') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($callOperators as $callOperator): ?>
            <tr>
                <td><?= $this->Number->format($callOperator->id) ?></td>
                <td><?= h($callOperator->name) ?></td>
                <td><?= h($callOperator->username) ?></td>
                <td><?= h($callOperator->password) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $callOperator->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $callOperator->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $callOperator->id], ['confirm' => __('Are you sure you want to delete # {0}?', $callOperator->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
