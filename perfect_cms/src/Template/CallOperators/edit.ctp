<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $callOperator->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $callOperator->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Call Operators'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Incident'), ['controller' => 'Incidents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="callOperators form large-9 medium-8 columns content">
    <?= $this->Form->create($callOperator) ?>
    <fieldset>
        <legend><?= __('Edit Call Operator') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
