<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Incident'), ['action' => 'edit', $incident->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Incident'), ['action' => 'delete', $incident->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incident Types'), ['controller' => 'IncidentTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident Type'), ['controller' => 'IncidentTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Call Operators'), ['controller' => 'CallOperators', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Call Operator'), ['controller' => 'CallOperators', 'action' => 'add']) ?> </li>
    </ul>
</nav>
-->


<div class="assistance view large-9 medium-8 columns content">
    <h3><?= h($assistanceType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Assistance Type') ?></th>
            <td><?= h($assistanceType->name) ?></td>
        </tr>
       
     
        <tr>
            <th><?= __('Agency') ?></th>
            <td><?= $assistanceType->has('agency') ? $this->Html->link($assistanceType->agency->name, ['controller' => 'Agencies', 'action' => 'view', $assistanceType->agency->id]) : '' ?></td>
        </tr>
       
    </table>

    
</div>
