<?= $this->element('content-header', ['title' => 'List of Assistance Type Records']);
    $this->fetch('content-header'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Type</th>     
                                <th>Agency</th>
                                <th>Assistance Hotline</th>                     
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- if there are no assistance type, i will print a message -->
                        <?php if ( empty($assistanceTypes) ) :
                                echo "There are no assistance type records.";
                              else :
                                foreach ($assistanceTypes as $assistanceType) : ?>
                                <tr>
                                    <td><?= $assistanceType->name ?></td>
                                    <td><?= $assistanceType->has('agency') ? $assistanceType->agency->name : '' ?>
                                    </td>
                                    <td><?= $assistanceType->number ?></td>
                                    <td>
                                        <?= $this->Form->button('Update', [ 'type' => 'submit', 
                                                                            'class' => 'btn btn-warning', 
                                                                            'onclick' => 'window.location.href=\''. $this->Url->build(['action' => 'edit', $assistanceType->id ]) .'\';']) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $assistanceType->id], ['class'=>'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $assistanceType->id)]) ?>
                                    </td>
                                </tr>
                        <?php   endforeach;
                              endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <script>
        $(function () {
          $("#example1").DataTable();
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
          });
        });
    </script>
</section>


