<?= $this->element('content-header', ['title' => 'Update Assistance Type']);
    $this->fetch('content-header'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Assistance Type</h3>
                </div>
                <?= $this->Form->create($assistanceType, ['class' => 'form-horizontal']) ?>
                <!-- form class="form-horizontal" -->
                    <div class="box-body">
                            <div class="form-group">
                            <label for="assistanceName" class="col-sm-4 control-label"> Name</label>
                            <div class="col-sm-6">
                                <?= $this->Form->input('name', [ 'label' => false, 
                                                                'class' => 'form-control', 
                                                                'placeholder' => 'e.g. Dengue Extermination' ]); ?>
                            </div>
                        </div>
                    <div class="form-group">
                            <label for="agency" class="col-sm-4 control-label">Agency</label>
                            <div class="col-sm-4">
                                <?= $this->Form->input('agency_id', [ 'label' => false, 
                                                                            'class' => 'form-control', 
                                                                            'options' => $agencies]); ?>
                            </div>
                    </div>

                       
                        
                        
                            
               
                        
                    </div>
                    
                    
                    
                    <div class="box-footer">
                        <?= $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-default']) ?>
                        <!-- button type="submit" class="btn btn-default">Reset</button -->
                        <?= $this->Form->button('Update', ['type' => 'submit', 'class' => 'btn btn-success pull-right']) ?>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
            
        <div class="clearfix visible-sm-block"></div>
    </div>
</section>

<!-- 
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incident->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incident Types'), ['controller' => 'IncidentTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Incident Type'), ['controller' => 'IncidentTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Call Operators'), ['controller' => 'CallOperators', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Call Operator'), ['controller' => 'CallOperators', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incidents form large-9 medium-8 columns content">
    <?= $this->Form->create($incident) ?>
    <fieldset>
        <legend><?= __('Edit Incident') ?></legend>
        <?php
            echo $this->Form->input('location');
            echo $this->Form->input('description');
            echo $this->Form->input('status');
            echo $this->Form->input('reporter_name');
            echo $this->Form->input('reporter_mobile');
            echo $this->Form->input('datetime');
            echo $this->Form->input('incident_type_id', ['options' => $incidentTypes]);
            echo $this->Form->input('call_operator_id', ['options' => $callOperators]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
-->