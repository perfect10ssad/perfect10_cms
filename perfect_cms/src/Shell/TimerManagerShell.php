<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\BroadcastManagerComponent; 
use App\Controller\Component\TwitterSenderComponent; 
use Cake\I18n\Time;
use Cake\Network\Http\Client;
use Cake\Utility\Xml;

class TimerManagerShell extends Shell
{
    public function main()
    {

    }

    public function initialize()
    {
        parent::initialize();
        // this model will be used to store the psi data
        $this->loadModel('Subscibers');
        $this->loadModel('HazePsiReadings');
        $this->loadModel('Incidents');
        $this->loadModel('IncidentTypes');
    }

    /**
    * This function will check if there are any
    * large differences between the past two PSI readings
    * 
    * The readings will be sent via email & SMS
    */
    public function comparePsiReadings(){
        $this->BroadcastManager = new BroadcastManagerComponent(new ComponentRegistry());

        $readings = $this->HazePsiReadings->find()->where(['region'=>'nRS'])->order(['reading_datetime' => 'DESC'])->extract('psi')->toArray();

        $http = new Client();
        $url = 'http://www.nea.gov.sg/api/WebAPI/?dataset=psi_update&keyref=781CF461BB6606AD62B1E1CAA87ECA61F2450EE51DF0DC33';
        $response = $http->get($url);
        $xml = Xml::toArray($response->xml);

        // 24 hour
        $twenty_fourPSI = $xml['channel']['item']['region'][1]['record']['reading'][0]['@value'];  
        // 3 hour
        $threePSI = $xml['channel']['item']['region'][1]['record']['reading'][1]['@value'];  

        $subject = "Hourly PSI update";
        $message = "Hourly PSI update. 3 Hour PSI: ".$threePSI.". 24 Hour PSI: ".$twenty_fourPSI;
        $this->BroadcastManager->broadcast($subject, $message);

        // this will be true if the values differ more than 51
        $airQualityChange = abs($readings[0] - $readings[1]) > 50;

        // for demo purposes
        $airQualityChange = true;

        $airQuality = array("Good", "Moderate", "Unhealthy", "Very Unhealthy", "Hazardous");
        if($airQualityChange) {
            $currentReading = $readings[0];
            $subject = "CMS PSI Alert";

            $message = "Regional PSI has changed. Please be advised to stay hydrated at all times. The current air quality is: ";
            if($currentReading <= 50) {
                $message = $message.$airQuality[0];
            } else if ($currentReading > 50 && $currentReading <= 100) {
                $message = $message.$airQuality[1];
            } else if ($currentReading > 100 && $currentReading <= 200) {
                $message = $message.$airQuality[2];
            } else if ($currentReading > 200 && $currentReading <= 300) {
                $message = $message.$airQuality[3];
            } else {
                $message = $message.$airQuality[4];
            }

            $this->BroadcastManager->broadcast($subject, $message);
        }
    }

    public function sendReport(){
        // this calls the method below to generate the pdf
        $this->generatePdf();

        $primeMinisterOfficeEmail = "perfect10_cms@zoho.com";
        //$primeMinisterOfficeEmail = "sereen89p@gmail.com";
        //$primeMinisterOfficeEmail = "hembubble@yahoo.com";
        $incidentTypes = $this->IncidentTypes->find();

        // find all incidents in the past 30 minutes
        $incidents = $this->Incidents->find()->where(['datetime >=' => new \DateTime('-30 minutes')]);
        $incidentCount = $incidents->count();

        $message = "Attached in this email is the summary report of the reported incidents for the past half an hour.<br/>
                    <p>In summary there are a total of ". $incidentCount ." incidents reported. The breakdown of the number of incidents are as follows:</p>";

        foreach($incidentTypes as $incidentType) {
            $iTypeCount = $incidents->where(['incident_type_id' => $incidentType->id])->count();
            $message = $message."<li>".$incidentType->name.": ".$iTypeCount."</li>";
            $incidents = $this->Incidents->find()->where(['datetime >=' => new \DateTime('-30 minutes')]);
        }

        $email = new Email('default');
        $email->from(['perfect10_cms@zoho.com' => 'Crisis Management System'])
            ->to($primeMinisterOfficeEmail)
            ->subject('Half-Hourly Crisis Report')
            ->emailFormat('html')
            ->attachments([
                'report.pdf' => [
                    'file' => APP . 'Generated_Reports' . DS . 'incidents.pdf',
                    'mimetype' => 'application/pdf'
                ]
            ])
            ->send('<p>Dear Prime Minister, </p>
                    <p>'.$message.'</p> 
                    <p>Regards,<br/>Perfect10 CMS Team</p>
                    <p>This is a system generated email, please do not reply to this email.');
    }

    public function generatePdf(){
        $haze_readings = $this->HazePsiReadings->find()->where(['region' => 'NRS'])->order(['reading_datetime' => 'DESC']);
        $incidents = $this->Incidents->find()->contain(['IncidentTypes'])->where(['datetime >=' => new \DateTime('-30 minutes')]);

        $nrs_psi_readings = $nrs_pm25_readings = $nrs_timings = $ce_psi = $no_psi = $we_psi = $so_psi = $ea_psi = array();
        $ce_pm25 = $no_pm25 = $we_pm25 = $ea_pm25 = $so_pm25 = array();

        foreach($haze_readings as $index => $n) {
            array_push($nrs_psi_readings, $n->psi);
            array_push($nrs_pm25_readings, $n->pm25);
            array_push($nrs_timings, $n->reading_datetime->i18nFormat('d MMM HH:mm'));

            if($index >= 9) 
                break;
        }

        $chart_psi_readings = $this->HazePsiReadings->find()->order(['reading_datetime' => 'ASC']);

        foreach($chart_psi_readings as $index=> $n) {
            switch ($n->region) {
                case "rCE":
                    array_push($ce_psi, $n->psi);
                    array_push($ce_pm25, $n->pm25);
                break;
                case "rNO":
                    array_push($no_psi, $n->psi);
                    array_push($no_pm25, $n->pm25);
                break;
                case "rWE":
                    array_push($we_psi, $n->psi);
                    array_push($we_pm25, $n->pm25);
                break;
                case "rEA":
                    array_push($ea_psi, $n->psi);
                    array_push($ea_pm25, $n->pm25);
                break;
                case "rSO":
                    array_push($so_psi, $n->psi);
                    array_push($so_pm25, $n->pm25);
                break;
            }

            if($index >= 60) 
                break;
        }

        $chart_psi = array('ce_psi'=>$ce_psi, 'no_psi'=>$no_psi, 'we_psi'=>$we_psi, 'ea_psi'=>$ea_psi, 'so_psi'=>$so_psi);
        $chart_pm25 = array('ce_pm25'=>$ce_pm25, 'no_pm25'=>$no_pm25, 'we_pm25'=>$we_pm25, 'ea_pm25'=>$ea_pm25, 'so_pm25'=>$so_pm25);

        $incidentTypes = $this->IncidentTypes->find();

        $CakePdf = new \CakePdf\Pdf\CakePdf();
        $CakePdf->template('view', 'default');
        $CakePdf->viewVars(array('incidents'=> $incidents, 
                                 'haze_readings'=>$haze_readings, 
                                  'nrs_psi_readings'=>$nrs_psi_readings, 
                                  'nrs_pm25_readings'=>$nrs_pm25_readings, 
                                  'nrs_timings'=>$nrs_timings,
                                    'chart_psi'=>$chart_psi,
                                    'chart_pm25'=>$chart_pm25,
                                    'incidentTypes'=>$incidentTypes));
        // Or write it to file directly
        $pdf = $CakePdf->write(APP . 'Generated_Reports' . DS . 'incidents.pdf');
    }
}