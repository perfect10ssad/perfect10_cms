<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Network\Http\Client;
use Cake\Utility\Xml;
use Cake\I18n\Time;

class DataCollectorShell extends Shell
{
// this class goes into the NEA API and collects the data from their hourly API
	
	public function initialize()
    {
        parent::initialize();
        // this model will be used to store the psi data
        $this->loadModel('HazePsiReadings');
    }

    public function main()
    {
        $http = new Client();
        $url = 'http://www.nea.gov.sg/api/WebAPI/?dataset=psi_update&keyref=781CF461BB6606AD62B1E1CAA87ECA61F2450EE51DF0DC33';
        $response = $http->get($url);
        $xml = Xml::toArray($response->xml);


        // get hourly 24 hour PSI
        for ($x = 0; $x <= 5; $x++) {
            $reading = $this->HazePsiReadings->newEntity();

            $reading->region = $xml['channel']['item']['region'][$x]['id'];
            //$datetime = Time::now();
            $datetime = $xml['channel']['item']['region'][$x]['record']['@timestamp'];  

            $datetime = substr($datetime, 0, 4). "-" . // year
                        substr($datetime, 4, 2). "-" . // month
                        substr($datetime, 6, 2). " " . // day
                        substr($datetime, 8, 2). ":" . // hour
                        substr($datetime, 10, 2). ":" . // min
                        substr($datetime, 12, 2); // ms
            $reading->reading_datetime = new Time($datetime
                                                    , "Asia/Singapore");

            // [0] takes the PSI. see the documentation to know more
            $reading->psi = $xml['channel']['item']['region'][$x]['record']['reading'][0]['@value'];  
            $reading->pm25 = $xml['channel']['item']['region'][$x]['record']['reading'][4]['@value'];  

            echo $reading->psi;

            // save each reading without caring about the results
            $this->HazePsiReadings->save($reading);
        } 
    }
}