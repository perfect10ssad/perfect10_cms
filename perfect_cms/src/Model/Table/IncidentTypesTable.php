<?php
namespace App\Model\Table;

use App\Model\Entity\IncidentType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IncidentTypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Incidents
 * @property \Cake\ORM\Association\BelongsToMany $AssistanceTypes
 */
class IncidentTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('incident_types');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Incidents', [
            'foreignKey' => 'incident_type_id'
        ]);
        $this->belongsToMany('AssistanceTypes', [
            'foreignKey' => 'incident_type_id',
            'targetForeignKey' => 'assistance_type_id',
            'joinTable' => 'incident_types_assistance_types'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
