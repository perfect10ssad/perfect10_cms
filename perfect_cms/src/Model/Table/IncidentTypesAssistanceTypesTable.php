<?php
namespace App\Model\Table;

use App\Model\Entity\IncidentTypesAssistanceType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IncidentTypesAssistanceTypes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $IncidentTypes
 * @property \Cake\ORM\Association\BelongsTo $AssistanceTypes
 */
class IncidentTypesAssistanceTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('incident_types_assistance_types');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('IncidentTypes', [
            'foreignKey' => 'incident_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AssistanceTypes', [
            'foreignKey' => 'assistance_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['incident_type_id'], 'IncidentTypes'));
        $rules->add($rules->existsIn(['assistance_type_id'], 'AssistanceTypes'));
        return $rules;
    }
}
