<?php
namespace App\Model\Table;

use App\Model\Entity\Incident;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Incidents Model
 *
 * @property \Cake\ORM\Association\BelongsTo $IncidentTypes
 * @property \Cake\ORM\Association\BelongsTo $CallOperators
 */
class IncidentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('incidents');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('IncidentTypes', [
            'foreignKey' => 'incident_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CallOperators', [
            'foreignKey' => 'call_operator_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->allowEmpty('status');

        $validator
            ->requirePresence('reporter_name', 'create')
            ->notEmpty('reporter_name');

        $validator
            ->requirePresence('reporter_mobile', 'create')
            ->notEmpty('reporter_mobile');

        $validator
            ->add('datetime', 'valid', ['rule' => 'datetime'])
            ->requirePresence('datetime', 'create')
            ->notEmpty('datetime');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['incident_type_id'], 'IncidentTypes'));
        $rules->add($rules->existsIn(['call_operator_id'], 'CallOperators'));
        return $rules;
    }
}
