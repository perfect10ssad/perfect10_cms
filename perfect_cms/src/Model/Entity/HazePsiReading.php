<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Network\Http\Client;
use Cake\Utility\Xml;
use Cake\I18n\Time;

/**
 * HazePsiReading Entity.
 *
 * @property string $region
 * @property \Cake\I18n\Time $reading_datetime
 * @property int $psi
 * @property int $pm25
 */
class HazePsiReading extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'region' => false,
        'reading_datetime' => false,
    ];
}
