<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * IncidentTypesAssistanceType Entity.
 *
 * @property int $id
 * @property int $incident_type_id
 * @property \App\Model\Entity\IncidentType $incident_type
 * @property int $assistance_type_id
 * @property \App\Model\Entity\AssistanceType $assistance_type
 */
class IncidentTypesAssistanceType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
