<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Incident Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $location
 * @property string $description
 * @property string $status
 * @property string $reporter_name
 * @property string $reporter_mobile
 * @property \Cake\I18n\Time $datetime
 * @property int $incident_type_id
 * @property \App\Model\Entity\IncidentType $incident_type
 * @property int $call_operator_id
 * @property \App\Model\Entity\CallOperator $call_operator
 */
class Incident extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
