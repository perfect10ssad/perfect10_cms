# ======= Web App Installation ============= #

1. Move the perfect_cms folder to your apache root folder.
2. Go to your phpMyAdmin (or any SQL manager)
3. Create a new user in your phpMyAdmin
4. Go to Users tab
5. There would be a box with a ‘New’ word on top, click ’Add user’
6. Create the user details with the following details:
 	* username: perfect10 (with dropdown use text field: selected)
 	* host: localhost
 	* password: vtX19[P4S)
 	* Database for user:
 	 	* Tick Create database with same name and grant all privileges.
7. Press GO and the user will be created
8. Import the SQL file into the db (using phpMyAdmin):
 	* It should create a database named perfect10.
 	* If it returns any error, create a new database named perfect10 and import the SQL file into that database.
9. Navigate to http://localhost/perfect10_cms/perfect_cms folder and the website should be able to load.

### Call operator login details ###
* username: shouxian 
* password: test

### PDF Generation ###
This project depends on the **[WkHtmlToPdf](http://wkhtmltopdf.org/)** library to create it's PDFs. Please ensure that this library is installed in the system to be able to generate the pdf files. In the case where you do not wish to use this binary, there has been a generated report under /src/Generated_Reports folder. The files have been configured to work on a UNIX based OS.

# ======= How to setup the timer classes? =========== #
*Note: A computer with bash command line is required for this*

The TimerManager uses OS's job scheduler to schedule the timer tasks. CakePHP has made it possible to run PHP scripts under a CLI environment by using the PHP-CGI binary together with the Shell framework provided by CakePHP.

These files belong to the Shell category under the CakePHP folders. There are mainly 2 timer classes which are inside of the Shell folder. TimerManagerShell and DataCollectorShell (PSI Retriever). These two shells has each of their functions called when the OS hits a certain timing along with it's scheduler.

To add the following components to run with the OS's timer, add the following lines to your crontab config.

First type into terminal
```
#!bash
crontab -e

```
Then add the following lines into the editor (press I to edit in vim)
```
#!bash
@hourly /path/to/php/php-cgi /path/to/apache/www/perfect10_cms/perfect_cms/bin/cake.php timer_manager compare_psi_readings
@hourly /path/to/php/php-cgi /path/to/apache/www/perfect10_cms/perfect_cms/bin/cake.php data_collector
*/30 * * * * /path/to/php/php-cgi /path/to/apache/www/perfect10_cms/perfect_cms/bin/cake.php timer_manager send_report

```
Then proceed to press ESC and type :x! to override the file

The timer should be running fine after this command has been executed

# ======= Problems you might encounter ====== #

Q: No intl.dll installed.

A: You must either enable it via AMPPS or you must install intl.dll to your php under your apache folder in order for cakePHP to work. Check out http://stackoverflow.com/questions/1451468/intl-extension-installing-php-intl-dll 